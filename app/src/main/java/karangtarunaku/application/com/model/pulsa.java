package karangtarunaku.application.com.model;

import java.io.Serializable;

public class pulsa implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id_pulsa, nominal;
    private String kode_pulsa, nama_pulsa, kode_service, nama_service, etd, tarif, gambar;
    private boolean is_selected;

    public pulsa(int id_pulsa, String kode_pulsa, String nama_pulsa, String kode_service, String nama_service, int nominal, String etd, String tarif, String gambar) {
        this.id_pulsa = id_pulsa;
        this.kode_pulsa = kode_pulsa;
        this.nama_pulsa = nama_pulsa;
        this.kode_service= kode_service;
        this.nama_service = nama_service;
        this.nominal = nominal;
        this.etd = etd;
        this.tarif = tarif;
        this.gambar = gambar;
    }

    public int getId_pulsa() {
        return this.id_pulsa;
    }

    public String getKode_pulsa() {
        return this.kode_pulsa;
    }

    public String getNama_pulsa() {
        return this.nama_pulsa;
    }

    public String getKode_service() {
        return this.kode_service;
    }

    public String getNama_service() {
        return this.nama_service;
    }

    public int getNominal() {
        return this.nominal;
    }

    public String getEtd() {
        return this.etd;
    }

    public String getTarif() {
        return this.tarif;
    }

    public String getGambar() {
        return this.gambar;
    }

    public boolean getIs_selected() {
        return this.is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected=is_selected;
    }

}
