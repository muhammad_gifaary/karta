package karangtarunaku.application.com.model;

import java.io.Serializable;

public class tagihan_listrik implements Serializable {

    private static final long serialVersionUID = 1L;
    String id_pelanggan, customer, tarif, daya, periode;
    double tagihan, denda, admin, total;

    public tagihan_listrik(String id_pelanggan, String customer, String tarif, String daya, String periode, double tagihan, double denda, double admin, double total) {
        
        this.id_pelanggan = id_pelanggan;
        this.customer = customer;
        this.tarif = tarif;
        this.daya = daya;
        this.periode = periode;
        this.tagihan = tagihan;
        this.denda = denda;
        this.admin = admin;
        this.total = total;
    }

    public String getId_pelanggan() {
        return this.id_pelanggan;
    }

    public String getCustomer() {
        return this.customer;
    }

    public String getTarif() {
        return this.tarif;
    }

    public String getDaya() {
        return this.daya;
    }

    public String getPeriode() {
        return this.periode;
    }

    public double getTagihan() {
        return this.tagihan;
    }

    public double getDenda() {
        return this.denda;
    }

    public double getAdmin() {
        return this.admin;
    }

    public double getTotal() {
        return this.total;
    }

}
