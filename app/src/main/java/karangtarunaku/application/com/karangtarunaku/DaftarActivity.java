package karangtarunaku.application.com.karangtarunaku;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import customfonts.MyEditText;
import customfonts.MyTextView;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.DatabaseHandler;
import karangtarunaku.application.com.libs.JSONParser;
import karangtarunaku.application.com.libs.MCrypt;
import karangtarunaku.application.com.model.city;
import karangtarunaku.application.com.model.facebook;
import karangtarunaku.application.com.model.order;
import karangtarunaku.application.com.model.province;
import karangtarunaku.application.com.model.subdistrict;
import karangtarunaku.application.com.model.user;

import static karangtarunaku.application.com.libs.CommonUtilities.getOptionsImage;
import static karangtarunaku.application.com.libs.CommonUtilities.initImageLoader;

public class DaftarActivity extends AppCompatActivity {

    //ImageView back;
    MyTextView signup, term_kondisi, or;
    CheckBox setuju;

    int province_id = 0;
    int city_id = 0;
    int subdistrict_id = 0;
    MyEditText edit_province;
    MyEditText edit_city;
    MyEditText edit_state;

    ArrayList<province> listProvince = new ArrayList<>();
    ArrayList<city> listCity = new ArrayList<>();
    ArrayList<subdistrict> listSubDistrict = new ArrayList<>();

    float downX = 0, downY = 0, upX, upY;
    Dialog dialog_listview;
    ListView listview;
    String action;

    Dialog dialog_informasi;
    MyTextView btn_ok;
    MyTextView text_title;
    MyTextView text_informasi;

    boolean success_daftar = false;
    //MyEditText firstname;
    //MyEditText lastname;
    MyEditText nama;
    MyEditText nohp;
    MyEditText email;
    MyEditText password;
    MyEditText konfirmasi;

    MyEditText edit_referensi;

    Dialog dialog_loading;

    user data;
    Context context;

    /*private facebook user_facebook;

    private CallbackManager callBackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    private LoginButton btnLoginFacebook;
    private FrameLayout frameLayoutLoginFacebook;*/

    //ImageView image_bg;
    //ImageLoader imageLoader;
    //DisplayImageOptions imageOptionBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = DaftarActivity.this;

        FacebookSdk.sdkInitialize(context);
        setContentView(R.layout.activity_daftar);

        edit_province = (MyEditText) findViewById(R.id.edit_provice);
        edit_city     = (MyEditText) findViewById(R.id.edit_city);
        edit_state    = (MyEditText) findViewById(R.id.edit_kecamatan);
        edit_referensi    = (MyEditText) findViewById(R.id.edit_referensi);

        edit_province.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downX = event.getX();
                        downY = event.getY();

                        break;

                    case MotionEvent.ACTION_UP:
                        upX = event.getX();
                        upY = event.getY();
                        float deltaX = downX - upX;
                        float deltaY = downY - upY;

                        if(Math.abs(deltaX)<50 && Math.abs(deltaY)<50) {
                            action = "province";
                            loadListArray();
                            dialog_listview.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog_listview.show();
                        }

                        break;
                }

                return false;
            }
        });

        edit_city.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downX = event.getX();
                        downY = event.getY();

                        break;

                    case MotionEvent.ACTION_UP:
                        upX = event.getX();
                        upY = event.getY();
                        float deltaX = downX - upX;
                        float deltaY = downY - upY;

                        if(Math.abs(deltaX)<50 && Math.abs(deltaY)<50) {
                            if(edit_province.getText().toString().length()==0) {
                                openDialogMessage("Propinsi tujuan harus diisi!", false);
                            } else {
                                loadDialogListView("city");
                            }
                        }

                        break;
                }

                return false;
            }
        });


        edit_state.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downX = event.getX();
                        downY = event.getY();

                        break;

                    case MotionEvent.ACTION_UP:
                        upX = event.getX();
                        upY = event.getY();
                        float deltaX = downX - upX;
                        float deltaY = downY - upY;

                        if(Math.abs(deltaX)<50 && Math.abs(deltaY)<50) {
                            if(edit_city.getText().toString().length()==0) {
                                openDialogMessage("Kabupaten / kota tujuan harus diisi!", false);
                            } else {
                                loadDialogListView("subdistrict");
                            }
                        }

                        break;
                }

                return false;
            }
        });

        dialog_listview = new Dialog(context);
        dialog_listview.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_listview.setCancelable(true);
        dialog_listview.setContentView(R.layout.list_dialog);

        listview = (ListView) dialog_listview.findViewById(R.id.listViewDialog);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog_listview.dismiss();
                if(action.equalsIgnoreCase("province")) {
                    edit_province.setText(listProvince.get(position).getProvince());
                    edit_city.setText("");
                    edit_state.setText("");

                    province_id = listProvince.get(position).getProvince_id();
                    city_id = 0;
                    subdistrict_id = 0;

                    new loadCity().execute();
                    new loadSubdistrict().execute();

                } else if(action.equalsIgnoreCase("city")) {
                    edit_city.setText(listCity.get(position).getCity());
                    edit_state.setText("");

                    city_id = listCity.get(position).getCity_id();
                    subdistrict_id = 0;

                    new loadSubdistrict().execute();

                } else if(action.equalsIgnoreCase("subdistrict")) {
                    edit_state.setText(listSubDistrict.get(position).getSubdistrict());
                    subdistrict_id = listSubDistrict.get(position).getSubdistrict_id();
                }
                action = "";
            }
        });

        new loadProvince().execute();



        //firstname = (MyEditText) findViewById(R.id.edit_firstname);
        //lastname  = (MyEditText) findViewById(R.id.edit_lastname);
        nama        = (MyEditText) findViewById(R.id.nama);
        nohp        = (MyEditText) findViewById(R.id.nohp);
        email       = (MyEditText) findViewById(R.id.email);
        password    = (MyEditText) findViewById(R.id.edit_password);
        konfirmasi  = (MyEditText) findViewById(R.id.edit_konfirmasi);

        signup = (MyTextView)findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(setuju.isChecked()) {
                    new prosesSingUp().execute();
                } else {
                    text_informasi.setText("Ceklist Syarat & Ketentuan!");
                    text_title.setText("KESALAHAN");
                    dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog_informasi.show();
                }
            }
        });

        or = (MyTextView) findViewById(R.id.or);
        term_kondisi = (MyTextView) findViewById(R.id.termc);
        term_kondisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TermKondisiActivity.class);
                startActivity(intent);
            }
        });

        setuju = (CheckBox) findViewById(R.id.checkbox_term);

        //progDailog = new ProgressDialog(context);
        //progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //progDailog.setCancelable(false);
        dialog_loading = new Dialog(context);
        dialog_loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_loading.setCancelable(false);
        dialog_loading.setContentView(R.layout.loading_dialog);
        //frame_loading = (FrameLayout) dialog_loading.findViewById(R.id.frame_loading);

        dialog_informasi = new Dialog(context);
        dialog_informasi.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_informasi.setCancelable(true);
        dialog_informasi.setContentView(R.layout.informasi_dialog);

        btn_ok = (MyTextView) dialog_informasi.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog_informasi.dismiss();
                if(success_daftar) {
                    Intent i = new Intent();
                    i.putExtra("is_login", true);
                    setResult(RESULT_OK, i);
                    finish();
                }
            }
        });

        text_title = (MyTextView) dialog_informasi.findViewById(R.id.text_title);
        text_informasi = (MyTextView) dialog_informasi.findViewById(R.id.text_dialog);
    }

    public void openDialogMessage(String message, boolean status) {
        text_informasi.setText(message);
        text_title.setText(status?"BERHASIL":"KESALAHAN");
        dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_informasi.show();
    }

    public void loadDialogListView(String act) {
        action = act;
        if(action.equalsIgnoreCase("province") && listProvince.size()==0) {
            openDialogLoadingEkspedisi();
        } else if(action.equalsIgnoreCase("city") && listCity.size()==0) {
            openDialogLoadingEkspedisi();
        } else if(action.equalsIgnoreCase("subdistrict") && listSubDistrict.size()==0) {
            openDialogLoadingEkspedisi();
        } else {
            loadListArray();
            dialog_listview.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog_listview.show();
        }
    }

    public void openDialogLoadingEkspedisi() {
        dialog_loading.setCancelable(true);
        dialog_loading.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_loading.show();
    }


    public void openDialogLoading() {
        dialog_loading.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_loading.show();
    }

    @Override
    protected void onResume() {
        registerReceiver(mHandleLoadEkspedisiReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.LOAD_EXPEDISI_LIST"));

        super.onResume();
    }

    private final BroadcastReceiver mHandleLoadEkspedisiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(dialog_loading.isShowing()) {
                loadListArray();
                dialog_loading.dismiss();
                dialog_listview.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_listview.show();
            }
        }
    };



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    class prosesSingUp extends AsyncTask<String, Void, JSONObject> {

        boolean success;
        String message;
        int aktivasi_sms;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            openDialogLoading();
            //.setMessage("Sign Up...");
            //progDailog.show();

        }

        @Override
        protected JSONObject doInBackground(String... urls) {
            JSONParser token_json = new JSONParser();
            JSONObject token = token_json.getJSONFromUrl(CommonUtilities.SERVER_URL + "/store/token.php", null, null);
            String cookies = token_json.getCookies();

            String security_code = "";
            try {
                security_code = token.isNull("security_code")?"":token.getString("security_code");
                MCrypt mCrypt = new MCrypt();
                security_code = new String(mCrypt.decrypt(security_code));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            JSONObject jObj = null;
            if(security_code.length()>0) {
                try {
                    String url = CommonUtilities.SERVER_URL + "/store/androidSignup.php";
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(url);

                    String nama_lengkap = nama.getText().toString().trim();
                    String[] temps = nama_lengkap.split(" ");
                    String firstname = temps[0];
                    String lastname  = "";
                    for(int i=1; i<temps.length; i++) {
                        lastname+=temps[i]+" ";
                    }
                    lastname = lastname.trim();

                    MultipartEntity reqEntity = new MultipartEntity();
                    reqEntity.addPart("first_name", new StringBody(firstname));
                    reqEntity.addPart("last_name", new StringBody(lastname));
                    reqEntity.addPart("email", new StringBody(email.getText().toString()));
                    reqEntity.addPart("nohp", new StringBody(nohp.getText().toString()));
                    reqEntity.addPart("password", new StringBody(password.getText().toString()));
                    reqEntity.addPart("konfirmasi", new StringBody(konfirmasi.getText().toString()));
                    //reqEntity.addPart("user_picture", new StringBody(user_facebook!=null?user_facebook.getUserpicture():""));
                    reqEntity.addPart("security_code", new StringBody(security_code));

                    reqEntity.addPart("id_propinsi", new StringBody(province_id+""));
                    reqEntity.addPart("nama_propinsi", new StringBody(edit_province.getText().toString()));
                    reqEntity.addPart("id_kota", new StringBody(city_id+""));
                    reqEntity.addPart("nama_kota", new StringBody(edit_city.getText().toString()));
                    reqEntity.addPart("id_kecamatan", new StringBody(subdistrict_id+""));
                    reqEntity.addPart("nama_kecamatan", new StringBody(edit_state.getText().toString()));
                    reqEntity.addPart("referensi", new StringBody(edit_referensi.getText().toString()));

                    httppost.setHeader("Cookie", cookies);
                    httppost.setEntity(reqEntity);
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity resEntity = response.getEntity();
                    InputStream is = resEntity.getContent();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;


                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    String json = sb.toString();
                    System.out.println(json);

                    jObj = new JSONObject(json);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();


                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return jObj;
        }

        @Deprecated
        @Override
        protected void onPostExecute(JSONObject result) {

            dialog_loading.dismiss();

            success = false;
            message = "Proses sign up gagal.";
            aktivasi_sms = 1;
            if(result!=null) {
                try {
                    success = result.isNull("success")?false:result.getBoolean("success");
                    message = result.isNull("message")?message:result.getString("message");
                    aktivasi_sms = result.isNull("aktivasi_sms")?aktivasi_sms:result.getInt("aktivasi_sms");
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if(success) {
                /*Intent i = new Intent();
                i.putExtra("is_login", true);
                setResult(RESULT_OK, i);
                finish();*/
                success_daftar = true;
                text_informasi.setText("Proses pendaftaran berhasil.");
                text_title.setText("BERHASIL");
                dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_informasi.show();
            } else {
                text_informasi.setText(message);
                text_title.setText("KESALAHAN");
                dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_informasi.show();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(mHandleLoadEkspedisiReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(mHandleLoadEkspedisiReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPause();
    }

    public class loadProvince extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... urls) {

            listProvince = new ArrayList<>();
            String url = CommonUtilities.SERVER_URL + "/store/androidPropinsiDataStore.php";
            JSONObject json = new JSONParser().getJSONFromUrl(url, null, null);
            if(json!=null) {
                try {
                    JSONArray data = json.isNull("topics")?null:json.getJSONArray("topics");
                    for (int i=0; i<data.length(); i++) {
                        JSONObject rec= data.getJSONObject(i);

                        int province_id = rec.isNull("province_id")?0:rec.getInt("province_id");
                        String province = rec.isNull("province")?"":rec.getString("province");

                        listProvince.add(new province(province_id, province));
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_EXPEDISI_LIST");
            sendBroadcast(i);

            return null;
        }
    }

    public class loadCity extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... urls) {

            listCity = new ArrayList<>();
            if(province_id>0) {
                String url = CommonUtilities.SERVER_URL + "/store/androidCityDataStore.php";
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("province_id", province_id+""));
                JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
                if(json!=null) {
                    try {
                        JSONArray data = json.isNull("topics")?null:json.getJSONArray("topics");
                        for (int i=0; i<data.length(); i++) {
                            JSONObject rec= data.getJSONObject(i);

                            int city_id = rec.isNull("city_id")?0:rec.getInt("city_id");
                            int province_id = rec.isNull("province_id")?0:rec.getInt("province_id");
                            String city = rec.isNull("city_name")?"":rec.getString("city_name");

                            listCity.add(new city(city_id, province_id, city));
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_EXPEDISI_LIST");
            sendBroadcast(i);

            return null;
        }
    }

    public class loadSubdistrict extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... urls) {

            listSubDistrict = new ArrayList<>();
            if(city_id>0) {
                String url = CommonUtilities.SERVER_URL + "/store/androidSubdistrictDataStore.php";
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("city_id", city_id+""));
                JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
                if(json!=null) {
                    try {
                        JSONArray data = json.isNull("topics")?null:json.getJSONArray("topics");
                        for (int i=0; i<data.length(); i++) {
                            JSONObject rec= data.getJSONObject(i);

                            int subdistrict_id = rec.isNull("subdistrict_id")?0:rec.getInt("subdistrict_id");
                            int city_id = rec.isNull("city_id")?0:rec.getInt("city_id");
                            String subdistrict = rec.isNull("subdistrict_name")?"":rec.getString("subdistrict_name");

                            listSubDistrict.add(new subdistrict(subdistrict_id, city_id, subdistrict));
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_EXPEDISI_LIST");
            sendBroadcast(i);

            return null;
        }
    }

    private void loadListArray() {
        String[] from = new String[] { getResources().getString(R.string.list_dialog_title) };
        int[] to = new int[] { R.id.txt_title };

        List<HashMap<String, String>> fillMaps = new ArrayList<HashMap<String, String>>();
        if(action.equalsIgnoreCase("province")) {
            for (province data : listProvince) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(getResources().getString(R.string.list_dialog_title), data.getProvince());

                fillMaps.add(map);
            }
        } else if(action.equalsIgnoreCase("city")) {
            for (city data : listCity) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(getResources().getString(R.string.list_dialog_title), data.getCity());

                fillMaps.add(map);
            }
        } else if(action.equalsIgnoreCase("subdistrict")) {
            for (subdistrict data : listSubDistrict) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(getResources().getString(R.string.list_dialog_title), data.getSubdistrict());

                fillMaps.add(map);
            }
        }

        SimpleAdapter adapter = new SimpleAdapter(context, fillMaps, R.layout.item_list_dialog, from, to);
        listview.setAdapter(adapter);
    }

}
