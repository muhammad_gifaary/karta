package karangtarunaku.application.com.karangtarunaku;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;

import java.util.ArrayList;
import java.util.List;

import customfonts.MyEditText;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.DatabaseHandler;
import karangtarunaku.application.com.libs.JSONParser;
import karangtarunaku.application.com.model.news;
import karangtarunaku.application.com.model.order;
import karangtarunaku.application.com.model.user;

public class DetailKartanewsActivity extends AppCompatActivity {

	Context context;

	ImageView back;
	LinearLayout linear_share;
	WebView webView;
	ProgressBar loading;
	LinearLayout retry;
	Button btnReload;
	MyEditText edit_komen;
	Button btn_komen;
    news kartanews;
	Boolean success;
	user data;
	LinearLayout ll_komen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kartanews);

		context = DetailKartanewsActivity.this;
		data = CommonUtilities.getSettingUser(context);

		webView = (WebView) findViewById(R.id.webview);
		loading = (ProgressBar) findViewById(R.id.pgbarLoading);
		retry = (LinearLayout) findViewById(R.id.loadMask);
		edit_komen = (MyEditText)findViewById(R.id.edit_komen);
		btn_komen = (Button)findViewById(R.id.btn_komen);
		btnReload = (Button) findViewById(R.id.btnReload);
		back = (ImageView) findViewById(R.id.back);
		linear_share = (LinearLayout) findViewById(R.id.toolbar_layout_share);
		ll_komen = (LinearLayout) findViewById(R.id.ll_komen);

		linear_share.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				String shareBody = CommonUtilities.SERVER_URL+"/adminweb/kartanews/detail.php?id="+kartanews.getId();
				Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
				startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.app_name)));
			}
		});

		btnReload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				loadDetail();
			}
		});

		btn_komen.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(!edit_komen.getText().toString().isEmpty()){
					new prosesComment().execute();
				}else{
					Toast.makeText(context,"Komentar wajib di isi",Toast.LENGTH_SHORT).show();
				}
			}
		});
        if(savedInstanceState==null) {
			kartanews = (news) getIntent().getSerializableExtra("kartanews");
        }

		loadDetail();

		webView.setVerticalScrollBarEnabled(false);
		webView.setWebChromeClient(new MyWebViewClient());

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});

		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_OK, new Intent());
				finish();
			}
		});
    }

	private class MyWebViewClient extends WebChromeClient {
		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			if(newProgress==100) {
				loading.setVisibility(View.GONE);
				ll_komen.setVisibility(View.VISIBLE);
			}
			super.onProgressChanged(view, newProgress);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			setResult(RESULT_OK, new Intent());
			finish();

			return false;
		}

		return super.onKeyDown(keyCode, event);
	}

	private void loadDetail() {
		loading.setVisibility(View.VISIBLE);
		retry.setVisibility(View.GONE);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setDomStorageEnabled(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);


		CookieSyncManager.createInstance(webView.getContext());
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);
		cookieManager.removeAllCookie(); //remove

		String url = CommonUtilities.SERVER_URL+"/adminweb/kartanews/detail.php?id="+kartanews.getId();
		webView.loadUrl(url);
	}

	class prosesComment extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... strings) {

			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("komen", edit_komen.getText().toString()));
			params.add(new BasicNameValuePair("id_karta", kartanews.getId()+""));
			if(data!=null){
				params.add(new BasicNameValuePair("user_name", data.getFirst_name()+" "+data.getLast_name()));
			}else{
				params.add(new BasicNameValuePair("user_name", "Guest"));
			}

			edit_komen.setText("");

			String url = CommonUtilities.SERVER_URL + "/store/androidSaveKomen.php";
			JSONObject result = new JSONParser().getJSONFromUrl(url, params,null);

			if (result != null) {
				try {
					success = result.isNull("success") ? false : result.getBoolean("success");
					if (success) {
						//loadDetail();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return success;
		}

		@Override
		protected void onPostExecute(Boolean aVoid) {
			super.onPostExecute(aVoid);


			if(success) {
				/*Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
				Intent intent = new Intent();
				intent.putExtra("alamat", data_selected_alamat);
				setResult(RESULT_OK, intent);
				finish();*/
				Toast.makeText(context,"Sukses menambahkan komentar",Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(context,"Terjadi Kesalahan",Toast.LENGTH_SHORT).show();
			}


		}
	}
	
}

