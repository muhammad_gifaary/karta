package karangtarunaku.application.com.karangtarunaku;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import customfonts.MyEditText;
import customfonts.MyTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import karangtarunaku.application.com.adapter.KomentarKartagramAdapter;
import karangtarunaku.application.com.adapter.MessageAdapter;
import karangtarunaku.application.com.adapter.PagerDetailKartagramAdapter;
import karangtarunaku.application.com.fragment.DetailKartagramFragment;
import karangtarunaku.application.com.fragment.KomentarKartagramFragment;
import karangtarunaku.application.com.libs.ChildAnimationExample;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.JSONParser;
import karangtarunaku.application.com.libs.SmallBang;
import karangtarunaku.application.com.libs.SmallBangListener;
import karangtarunaku.application.com.libs.SquareImageView;
import karangtarunaku.application.com.model.gallery;
import karangtarunaku.application.com.model.message;
import karangtarunaku.application.com.model.message_list;
import karangtarunaku.application.com.model.user;

@SuppressLint("InflateParams")
public class DetailKartagramActivity extends AppCompatActivity {
	
	Context context;

	public static ArrayList<message> listMessage;
	public static KomentarKartagramAdapter messageAdapter;

	public static LinearLayout llMsgCompose;
	public static ListView listViewMessage;
	public static MyEditText inputMsg;
	public static ImageView btn_send;
	public static ProgressBar progres_save;

	public static ProgressBar loading;
	public static LinearLayout retry;
	public static Button btnReload;
	

	Dialog dialog_informasi;
    MyTextView btn_ok;
    MyTextView text_title;
    MyTextView text_informasi;

	Boolean is_get_message = false;

	ImageView back;
	user data;
	gallery data_kartagram;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_kartagram);

		context = DetailKartagramActivity.this;
		data = CommonUtilities.getSettingUser(context);

		back            = (ImageView) findViewById(R.id.back);
		llMsgCompose    = (LinearLayout) findViewById(R.id.llMsgCompose);
		listViewMessage = (ListView) findViewById(R.id.listMessage);
		inputMsg        = (MyEditText) findViewById(R.id.inputMsg);
		btn_send        = (ImageView) findViewById(R.id.btnSend);
		progres_save    = (ProgressBar) findViewById(R.id.progres_save);

		loading         = (ProgressBar) findViewById(R.id.pgbarLoading);
		retry           = (LinearLayout) findViewById(R.id.loadMask);
		btnReload       = (Button) findViewById(R.id.btnReload);

		btnReload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				loadDataMessage();
			}
		});


		inputMsg.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEND) {
					addKomentar();
					return true;
				}
				return false;
			}
		});

		btn_send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				addKomentar();
			}
		});



		back.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) {
				finish();
			}
		});

        dialog_informasi = new Dialog(context);
        dialog_informasi.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_informasi.setCancelable(true);
        dialog_informasi.setContentView(R.layout.informasi_dialog);

        btn_ok = (MyTextView) dialog_informasi.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog_informasi.dismiss();
            }
        });

        text_title = (MyTextView) dialog_informasi.findViewById(R.id.text_title);
        text_informasi = (MyTextView) dialog_informasi.findViewById(R.id.text_dialog);

		listMessage = new ArrayList<>();
		messageAdapter = new KomentarKartagramAdapter(listMessage);
		listViewMessage.setAdapter(messageAdapter);

		if(savedInstanceState==null) {
			data_kartagram = (gallery) getIntent().getSerializableExtra("kartagram");
		}

		LayoutInflater inflater = getLayoutInflater();
		ViewGroup header = (ViewGroup) inflater.inflate(R.layout.kartagram_item_list, listViewMessage, false);

			CircleImageView photo_profile = (CircleImageView) header.findViewById(R.id.profile_photo);
			MyTextView username = (MyTextView) header.findViewById(R.id.username);
			MyTextView tambah = (MyTextView) header.findViewById(R.id.tambah);
			SquareImageView image = (SquareImageView) header.findViewById(R.id.post_image);
			LinearLayout linearLike = (LinearLayout) header.findViewById(R.id.linear_like);
			final ImageView fav1 = (ImageView) header.findViewById(R.id.fav1);
			final ImageView fav2 = (ImageView) header.findViewById(R.id.fav2);
			final SmallBang mSmallBang = SmallBang.attach2Window(DetailKartagramActivity.this);
			MyTextView likes = (MyTextView) header.findViewById(R.id.likes);
			MyTextView caption = (MyTextView) header.findViewById(R.id.caption);
			//MyTextView detail = (MyTextView) header.findViewById(R.id.detail);

			//view.linearLike.setVisibility(MainActivity.data.getId()>0?View.VISIBLE:View.GONE);
			String server = CommonUtilities.SERVER_URL;
			String url = server+"/store/centercrop_3.php?url="+ server+"/uploads/kartagram/"+data_kartagram.getGambar()+"&width=300&height=300";
			String url_user = server+"/store/centercrop_3.php?url="+ server+"/uploads/member/"+data_kartagram.getPhoto()+"&width=300&height=300";
			//String url = server+"/uploads/kartagram/"+data_kartagram.getGambar();
			MainActivity.imageLoader.displayImage(url, image, MainActivity.imageOptionKategori);
			MainActivity.imageLoader.displayImage(url_user, photo_profile, MainActivity.imageOptionsUser);
			username.setText(data_kartagram.getOwner());
			tambah.setVisibility(View.GONE);
			//detail.setVisibility(View.GONE);
			caption.setText(data_kartagram.getKeterangan());
			likes.setText(data_kartagram.getLike()>0?data_kartagram.getLike()+" Suka":"");
			fav1.setVisibility(data_kartagram.getFavorite()?View.GONE:View.VISIBLE);
			fav2.setVisibility(data_kartagram.getFavorite()?View.VISIBLE:View.GONE);

			fav1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(data.getId()==0) {
						String message = "Anda harus login dahulu!";
						text_informasi.setText(message);
						text_title.setText("KESALAHAN");
						dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
						dialog_informasi.show();
					} else {
						data_kartagram.setFavorite(true);
						new likeunlikeKartagram(1, data_kartagram.getId()).execute();

						fav2.setVisibility(View.VISIBLE);
						fav1.setVisibility(View.GONE);
						like(v);
					}
				}

				public void like(View v){
					fav2.setImageResource(R.drawable.f4);
					mSmallBang.bang(v);
					mSmallBang.setmListener(new SmallBangListener() {
						@Override
						public void onAnimationStart() {

						}

						@Override
						public void onAnimationEnd() {

						}


					});
				}
			});

			fav2.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(data.getId()==0) {
						String message = "Anda harus login dahulu!";
						text_informasi.setText(message);
						text_title.setText("KESALAHAN");
						dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
						dialog_informasi.show();
					} else {

						data_kartagram.setFavorite(false);
						new likeunlikeKartagram(0, data_kartagram.getId()).execute();

						fav2.setVisibility(View.GONE);
						fav1.setVisibility(View.VISIBLE);
					}
				}
			});

		listViewMessage.addHeaderView(header, null, false);


    }

	public void likeunlikeKartagram(int status, int id) {

		new likeunlikeKartagram(status, id).execute();
	}

	public class likeunlikeKartagram extends AsyncTask<String, Void, Void> {

		int id;
		int status = 0;
		boolean success;
		String message;

		likeunlikeKartagram(int status, int id) {
			this.status = status;
			this.id = id;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(String... urls) {

			String url = CommonUtilities.SERVER_URL + "/store/androidKartagramUpdateFavorite.php";
			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("id_user", data.getId()+""));
			params.add(new BasicNameValuePair("status", status+""));
			params.add(new BasicNameValuePair("id", id+""));

			JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
			if(json!=null) {
				try {
					success = json.isNull("success")?false:json.getBoolean("success");
					message = json.isNull("message")?"":json.getString("message");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			return null;
		}
	}

	@Override
	protected void onResume() {
		registerReceiver(mHandleLoadDataMessageReceiver,  new IntentFilter("karangtarunaku.application.com.karangtarunaku.LOAD_MESSAGE"));
		registerReceiver(mHandleNewMessageReceiver,  new IntentFilter("karangtarunaku.application.com.karangtarunaku.NEW_MESSAGE"));
		registerReceiver(mHandleSendMessageReceiver,  new IntentFilter("karangtarunaku.application.com.karangtarunaku.SEND_MESSAGE"));

		loadDataMessage();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		try {
			CommonUtilities.setCurrentDiskusiKartagram(context, new gallery(0, "", ""));
			unregisterReceiver(mHandleLoadDataMessageReceiver);
			unregisterReceiver(mHandleNewMessageReceiver);
			unregisterReceiver(mHandleSendMessageReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		super.onPause();
	}
	
	@Override
    protected void onDestroy() {
        super.onDestroy();

		try {
			CommonUtilities.setCurrentDiskusiKartagram(context, new gallery(0, "", ""));
			unregisterReceiver(mHandleLoadDataMessageReceiver);
			unregisterReceiver(mHandleNewMessageReceiver);
			unregisterReceiver(mHandleSendMessageReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public void playBeep() {

		try {
			Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(context, notification);
			r.play();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void appendMessage(message m) {
		listMessage.add(m);
		messageAdapter.UpdatemessageAdapter(listMessage);
		listViewMessage.setScrollY(View.FOCUS_DOWN);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent issue) {
		if (keyCode == KeyEvent.KEYCODE_BACK && issue.getRepeatCount() == 0) {

			Intent intent = new Intent();
			intent.putExtra("is_get_message", is_get_message);
			setResult(RESULT_OK, intent);
			finish();

			return false;
		}
		
		return super.onKeyDown(keyCode, issue);
	}

	public void loadDataMessage() {
		new loadDataMessage().execute();
	}
	
	public class loadDataMessage extends AsyncTask<String, Void, ArrayList<message>> {
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			loading.setVisibility(View.VISIBLE);
			retry.setVisibility(View.GONE);
			listViewMessage.setVisibility(View.GONE);
			llMsgCompose.setVisibility(View.GONE);
	    }
		 
		@Override
	    protected ArrayList<message> doInBackground(String... urls) {

			ArrayList<message> result = null;
			String url = CommonUtilities.SERVER_URL + "/store/androidKomentarKartagramDataStore.php";

			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("id_kartagram", data_kartagram.getId() + ""));
			params.add(new BasicNameValuePair("id_user", data.getId() + ""));
				JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);

				if(json!=null) {
					try {
						result = new ArrayList<>();
						JSONArray topics = json.isNull("topics")?null:json.getJSONArray("topics");
						for (int i=0; i<topics.length(); i++) {
							JSONObject rec = topics.getJSONObject(i);

							int id = rec.isNull("id")?null:rec.getInt("id");
							String nama = rec.isNull("nama")?null:rec.getString("nama");
							String telepon = rec.isNull("telepon")?null:rec.getString("telepon");
							String photo = rec.isNull("photo")?null:rec.getString("photo");

							String message = rec.isNull("message")?null:rec.getString("message");
							String datetime = rec.isNull("datetime")?null:rec.getString("datetime");
							Boolean is_self = rec.isNull("is_self")?false:rec.getBoolean("is_self");
							Boolean is_sent = rec.isNull("is_sent")?false:rec.getBoolean("is_sent");

							result.add(new message(id, nama, telepon, photo, datetime, message, is_self, is_sent));
						}

						CommonUtilities.setCurrentDiskusiKartagram(context, data_kartagram);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

	    	return result;
	    }

		
	    @Override
	    protected void onPostExecute(ArrayList<message> result) {
	    
	    	Boolean success = result!=null;
	    	if(result==null) result = new ArrayList<>();
	    	ArrayList<message_list> temp = new ArrayList<>();
	    	temp.add(new message_list(result));

			is_get_message = success;

	    	Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_MESSAGE");
	    	i.putExtra("message_list", temp);
	    	i.putExtra("success", success);
	    	sendBroadcast(i);	
	    }
	}

	private final BroadcastReceiver mHandleLoadDataMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Boolean success = intent.getBooleanExtra("success", false);
			ArrayList<message_list> temp = intent.getParcelableArrayListExtra("message_list");
			ArrayList<message> result = temp.get(0).getListData();

			loading.setVisibility(View.GONE);
			if(!success) {
				retry.setVisibility(View.VISIBLE);
			} else {
				listViewMessage.setVisibility(View.VISIBLE);
				llMsgCompose.setVisibility(View.VISIBLE);
			}

			for (message flist : result) {
				listMessage.add(flist);
			}

			messageAdapter.UpdatemessageAdapter(listMessage);
		}
	};

	private final BroadcastReceiver mHandleNewMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//Toast.makeText(context, "New Message!", Toast.LENGTH_SHORT).show();
			message m = (message) intent.getSerializableExtra("message");
			appendMessage(m);
			playBeep();
		}
	};

	private final BroadcastReceiver mHandleSendMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			message m = (message) intent.getSerializableExtra("message");
			appendMessage(m);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				Intent intent = new Intent();
				intent.putExtra("is_get_message", is_get_message);
				setResult(RESULT_OK, intent);
				finish();

				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void addKomentar() {
		if(data.getId()==0) {
			String message = "Anda harus login dahulu!";
			text_informasi.setText(message);
			text_title.setText("KESALAHAN");
			dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog_informasi.show();
		} else {
			new prosesAddKomentar().execute();
		}
	}

	class prosesAddKomentar extends AsyncTask<String, Void, JSONObject> {

		boolean success;
		String message;
		message datamsg;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			btn_send.setVisibility(View.GONE);
			progres_save.setVisibility(View.VISIBLE);
		}

		@Override
		protected JSONObject doInBackground(String... urls) {
			JSONParser token_json = new JSONParser();
			JSONObject token = token_json.getJSONFromUrl(CommonUtilities.SERVER_URL + "/store/token.php", null, null);
			String cookies = token_json.getCookies();

			String security_code = "";
			try {
				security_code = token.isNull("security_code")?"":token.getString("security_code");
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			JSONObject jObj = null;
			if(security_code.length()>0) {
				try {
					String url = CommonUtilities.SERVER_URL + "/store/androidAddKomentarKartagram.php";

					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(url);

					MultipartEntity reqEntity = new MultipartEntity();
					reqEntity.addPart("user_id", new StringBody(data.getId() + ""));
					reqEntity.addPart("id_kartagram", new StringBody(data_kartagram.getId()+""));
					reqEntity.addPart("isi_komentar", new StringBody(inputMsg.getText().toString()));
					reqEntity.addPart("security_code", new StringBody(security_code));

					httppost.setHeader("Cookie", cookies);
					httppost.setEntity(reqEntity);
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity resEntity = response.getEntity();
					InputStream is = resEntity.getContent();

					BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;


					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					is.close();
					String json = sb.toString();
					System.out.println(json);

					jObj = new JSONObject(json);

				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			return jObj;
		}

		@Deprecated
		@Override
		protected void onPostExecute(JSONObject result) {

			success = false;
			message = "Gagal proses kirim komentar. Cobalah lagi.";
			datamsg = null;
			if(result!=null) {
				try {
					success = result.isNull("success")?false:result.getBoolean("success");
					message = result.isNull("message")?message:result.getString("message");
					JSONObject rec = result.getJSONObject("data_message");

					int id = rec.isNull("id")?null:rec.getInt("id");
					String nama = rec.isNull("nama")?null:rec.getString("nama");
					String telepon = rec.isNull("telepon")?null:rec.getString("telepon");
					String photo = rec.isNull("photo")?null:rec.getString("photo");

					String message = rec.isNull("message")?null:rec.getString("message");
					String datetime = rec.isNull("datetime")?null:rec.getString("datetime");
					Boolean is_self = rec.isNull("is_self")?false:rec.getBoolean("is_self");
					Boolean is_sent = rec.isNull("is_sent")?false:rec.getBoolean("is_sent");

					datamsg = new message(id, nama, telepon, photo, datetime, message, is_self, is_sent);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(success) {
				Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.SEND_MESSAGE");
				i.putExtra("message", datamsg);
				sendBroadcast(i);

				inputMsg.setText("");
			} else {
                text_informasi.setText(message);
                text_title.setText("KESALAHAN");
                dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_informasi.show();
			}
			btn_send.setVisibility(View.VISIBLE);
			progres_save.setVisibility(View.GONE);
		}
	}

	public void LoadDetailKartagram() {
		String server = CommonUtilities.SERVER_URL;
		String url = server+"/uploads/kartagram/"+data_kartagram.getGambar();
		TextSliderView textSliderView = new TextSliderView(context);
		textSliderView
				//.description(name)
				.image(url)
				.setScaleType(BaseSliderView.ScaleType.CenterInside);
		textSliderView.bundle(new Bundle());

		DetailKartagramFragment.mProdukSlider.addSlider(textSliderView);

		//DetailKartagramFragment.mProdukSlider.setPresetTransformer(SliderLayout.Transformer.Default);
		DetailKartagramFragment.mProdukSlider.stopAutoCycle();

		DetailKartagramFragment.mProdukSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
		DetailKartagramFragment.mProdukSlider.setCustomAnimation(new ChildAnimationExample());
		DetailKartagramFragment.mProdukSlider.setDuration(0);

		DetailKartagramFragment.tanggal.setText(data_kartagram.getKeterangan());
		DetailKartagramFragment.oleh.setText(data_kartagram.getOwner());
	}
}
