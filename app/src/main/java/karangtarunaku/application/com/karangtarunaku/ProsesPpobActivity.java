package karangtarunaku.application.com.karangtarunaku;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import customfonts.MyTextView;
import karangtarunaku.application.com.adapter.ListDataAdapter;
import karangtarunaku.application.com.adapter.ListPulsaAdapter;
import karangtarunaku.application.com.adapter.ListTokenListrikAdapter;
import karangtarunaku.application.com.adapter.ListVoucherGameAdapter;
import karangtarunaku.application.com.adapter.PagerPpobAdapter;
import karangtarunaku.application.com.adapter.PesananAdapter;
import karangtarunaku.application.com.adapter.SelectBankPpobAdapter;
import karangtarunaku.application.com.fragment.BayarListrikFragment;
import karangtarunaku.application.com.fragment.BayarPdamFragment;
import karangtarunaku.application.com.fragment.BayarTelkomFragment;
import karangtarunaku.application.com.fragment.BeliVoucherGameFragment;
import karangtarunaku.application.com.fragment.BeliVoucherListrikFragment;
import karangtarunaku.application.com.fragment.DepositSaldoFragment;
import karangtarunaku.application.com.fragment.DonasiFragment;
import karangtarunaku.application.com.fragment.IsiDataFragment;
import karangtarunaku.application.com.fragment.IsiPulsaFragment;
import karangtarunaku.application.com.fragment.MetodePembayaranFragment;
import karangtarunaku.application.com.fragment.KonfirmasiPemesananFragment;
import karangtarunaku.application.com.fragment.ProsesBerhasilFragment;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.LockableViewPager;
import karangtarunaku.application.com.model.bank;
import karangtarunaku.application.com.model.game;
import karangtarunaku.application.com.model.order;
import karangtarunaku.application.com.model.pdam;
import karangtarunaku.application.com.model.produk;
import karangtarunaku.application.com.model.pulsa;
import karangtarunaku.application.com.model.tagihan_listrik;
import karangtarunaku.application.com.model.tagihan_pdam;
import karangtarunaku.application.com.model.tagihan_telkom;
import karangtarunaku.application.com.model.token_listrik;
import karangtarunaku.application.com.model.user;
import karangtarunaku.application.com.libs.JSONParser;
import karangtarunaku.application.com.libs.MCrypt;
import karangtarunaku.application.com.model.voucher_game;

import static karangtarunaku.application.com.libs.CommonUtilities.getOptionsImage;
import static karangtarunaku.application.com.libs.CommonUtilities.initImageLoader;

public class ProsesPpobActivity extends AppCompatActivity {

    Context context;
    user data;

    int ppob_id;
	bank data_bank;
	int bank_index = -1;
	double total_belanja = 0;
	int metode_pembayaran_id = 1;
	String no_transaksi;

	ImageView back;
	MyTextView title;
	LockableViewPager viewpager;

	LinearLayout retry;
	MyTextView btnReload;

	Dialog dialog_loading;

	Dialog dialog_informasi;
	MyTextView btn_ok;
	MyTextView text_title;
	MyTextView text_informasi;

	Dialog dialog_listview;
	ListView listview;

	// PULSA LIST
	ArrayList<pulsa> pulsalist = new ArrayList<>();
	ListPulsaAdapter pulsaAdapter;

	// DATA LIST
	ArrayList<pulsa> datalist = new ArrayList<>();
	ListDataAdapter dataAdapter;

	//TAGIHAN TELKOM
	tagihan_telkom tagihan_telkom_selectd;

	// TOKEN LIST
	token_listrik token_listrik_selectd;
	ArrayList<token_listrik> tokenlistriklist = new ArrayList<>();
	ListTokenListrikAdapter tokenlistrikAdapter;

	//TAGIHAN LISTRUK
	tagihan_listrik tagihan_listrik_selectd;

	//TAGIHAN PDAM
	tagihan_pdam tagihan_pdam_selectd;

	//BANK LIST
	ArrayList<bank> banklist = new ArrayList<>();
	SelectBankPpobAdapter selectBankAdapter;

	//PESANAN
	ArrayList<produk> pesananlist = new ArrayList<>();
	PesananAdapter pesananAdapter;

	public static ImageLoader imageLoader;
	public static DisplayImageOptions imageOptionPembayaran;
	public static DisplayImageOptions imageOptionPulsa;

	int page = 1;

	//PRODUK PULSA
	pulsa produk;
	String no_hp = "";
	String action;

	//GAMEONLINE
    game game_selected;
    ArrayList<game> listGame = new ArrayList<>();
    voucher_game voucher_game_selectd;
    
    ArrayList<voucher_game> vouchergamelist = new ArrayList<>();
    ListVoucherGameAdapter vouchergameAdapter;


	//PDAM
	ArrayList<pdam> listPdam = new ArrayList<>();
	pdam pdam_selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proses_ppob);

        context         = ProsesPpobActivity.this;
        data 			= CommonUtilities.getSettingUser(context);

        initImageLoader(context);
		imageLoader           = ImageLoader.getInstance();
		imageOptionPembayaran = getOptionsImage(R.drawable.logo_grayscale, R.drawable.logo_grayscale);
		imageOptionPembayaran = getOptionsImage(R.drawable.logo_grayscale, R.drawable.logo_grayscale);

		back      = (ImageView) findViewById(R.id.back);
		title     = (MyTextView) findViewById(R.id.title);
		viewpager = (LockableViewPager) findViewById(R.id.pager);
		viewpager.setSwipeLocked(true);

		retry = (LinearLayout) findViewById(R.id.loadMask);
		btnReload = (MyTextView) findViewById(R.id.btnReload);

		btnReload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				app_close();
			}
		});

		dialog_listview = new Dialog(context);
		dialog_listview.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog_listview.setCancelable(true);
		dialog_listview.setContentView(R.layout.list_dialog);

		listview = (ListView) dialog_listview.findViewById(R.id.listViewDialog);
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				dialog_listview.dismiss();
                if(action.equalsIgnoreCase("pdam")) {
                    pdam_selected = listPdam.get(position);
                    BayarPdamFragment.edit_jenis.setText(pdam_selected.getNama());
                } else if(action.equalsIgnoreCase("game")) {
                    game_selected = listGame.get(position);
                    BeliVoucherGameFragment.edit_jenis.setText(game_selected.getNama());
                }
				action = "";
			}
		});

		dialog_loading = new Dialog(context);
		dialog_loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog_loading.setCancelable(false);
		dialog_loading.setContentView(R.layout.loading_dialog);

		dialog_informasi = new Dialog(context);
		dialog_informasi.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog_informasi.setCancelable(true);
		dialog_informasi.setContentView(R.layout.informasi_dialog);

		btn_ok = (MyTextView) dialog_informasi.findViewById(R.id.btn_ok);
		btn_ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dialog_informasi.dismiss();
			}
		});

		text_title = (MyTextView) dialog_informasi.findViewById(R.id.text_title);
		text_informasi = (MyTextView) dialog_informasi.findViewById(R.id.text_dialog);

		if(savedInstanceState==null) {
			ppob_id = getIntent().getIntExtra("ppob_id", 0);
			title.setText(getIntent().getStringExtra("title"));
		}

		PagerPpobAdapter pagerPpobAdapter = new PagerPpobAdapter(ppob_id, getSupportFragmentManager());
		viewpager.setAdapter(pagerPpobAdapter);
	}

	public void beliData(pulsa produk) {
		String message = checkedIsiDataBeforeNext();
		if(message.length()==0) {
			this.produk = produk;
			no_hp = IsiDataFragment.edit_nohp.getText().toString();
			total_belanja = this.produk.getNominal();
			loadFieldTotalTransfer();
			gotoPage(2);
		} else {
			openDialogMessage(message, false);
		}
	}

	public void beliTokenListrik(token_listrik produk) {
		String message = checkedBeliVoucherListrikBeforeNext();
		if(message.length()==0) {
			token_listrik_selectd = produk;
			no_hp = BeliVoucherListrikFragment.edit_id_pel.getText().toString();
			total_belanja = token_listrik_selectd.getHarga();
			loadFieldTotalTransfer();
			gotoPage(2);
		} else {
			openDialogMessage(message, false);
		}
	}


    public void beliVoucherGame(voucher_game produk) {
        
            voucher_game_selectd = produk;
            total_belanja = voucher_game_selectd.getHarga();
            loadFieldTotalTransfer();
            gotoPage(2);
    }
    
	public void beliPulsa(pulsa produk) {
		String message = checkedIsiPulsaBeforeNext();
		if(message.length()==0) {
    		this.produk = produk;
    		no_hp = IsiPulsaFragment.edit_nohp.getText().toString();
			total_belanja = this.produk.getNominal();
			loadFieldTotalTransfer();
			gotoPage(2);
		} else {
			openDialogMessage(message, false);
		}
	}

    public void gotoPage(int page) {
    	this.page = page;
    	viewpager.setCurrentItem(page-1, true);
	}

    public void pickPhoneBook() {
    	Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, 1);
    }

    public void cekTagihanTelkom(String nohp) {
		String message = checkedBeforeCekTagihanTelkom();
		if(message.length()==0) {
			new prosesCekTagihanTelkom(nohp).execute();
		} else {
			openDialogMessage(message, false);
		}
	}

	public void cekTagihanListrik(String nohp) {
		String message = checkedBeforeCekTagihanListrik();
		if(message.length()==0) {
			new prosesCekTagihanListrik(nohp).execute();
		} else {
			openDialogMessage(message, false);
		}
	}

	public void cekTagihanPdam(String nohp) {
		String message = checkedBeforeCekTagihanPdam();
		if(message.length()==0) {
			new prosesCekTagihanPdam(nohp).execute();
		} else {
			openDialogMessage(message, false);
		}
	}

    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode == RESULT_OK) {
			switch(requestCode) {
				case 1:
					Uri contactData = data.getData();

					@SuppressWarnings("deprecation")
                    Cursor c =  managedQuery(contactData, null, null, null, null);
				    if (c.moveToFirst()) {
				    	String id =c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
				        String hasPhone =c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
				        if (hasPhone.equalsIgnoreCase("1")) {
				        	Cursor phones = getContentResolver().query(
				        		ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
				                ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,
				                null, null);
				            phones.moveToFirst();
				            String cNumber = phones.getString(phones.getColumnIndex("data1"));
				            //System.out.println("number is:"+cNumber);
							IsiPulsaFragment.edit_nohp.setText(cNumber.replace("+62", "0").replace("-", "").replace(" ", ""));
				        } else {
				        	openDialogMessage("Kontak tidak memiliki nomer.", false);
				        	//String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
					        //no_handphone.setText(name);
				        }
				     }

				    break;
			}
		}
	}

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			setResult(RESULT_OK, new Intent());
			finish();

		    return true;
        default:
            return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			app_close();

			return false;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onDestroy() {
		try {
            unregisterReceiver(mHandleLoadListBankReceiver);
            unregisterReceiver(mHandleLoadListPulsaReceiver);
            unregisterReceiver(mHandleLoadListDataReceiver);
            unregisterReceiver(mHandleLoadListTokenListrikReceiver);
            unregisterReceiver(mHandleLoadListVoucherGameReceiver);
            unregisterReceiver(mHandleLoadListPdamReceiver);
            unregisterReceiver(mHandleLoadListGameReceiver);

            unregisterReceiver(mHandleSubmitDepositReceiver);
            unregisterReceiver(mHandleSubmitPulsaReceiver);
            unregisterReceiver(mHandleSubmitDataReceiver);
            unregisterReceiver(mHandleSubmitDonasiReceiver);

            unregisterReceiver(mHandleCekTagihanTelkomReceiver);
            unregisterReceiver(mHandleCekTagihanListrikReceiver);
            unregisterReceiver(mHandleCekTagihanPdamReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.onDestroy();
	}

	@Override
	protected void onPause() {
		try {
			unregisterReceiver(mHandleLoadListBankReceiver);
			unregisterReceiver(mHandleLoadListPulsaReceiver);
			unregisterReceiver(mHandleLoadListDataReceiver);
			unregisterReceiver(mHandleLoadListTokenListrikReceiver);
            unregisterReceiver(mHandleLoadListVoucherGameReceiver);
			unregisterReceiver(mHandleLoadListPdamReceiver);
            unregisterReceiver(mHandleLoadListGameReceiver);

			unregisterReceiver(mHandleSubmitDepositReceiver);
			unregisterReceiver(mHandleSubmitPulsaReceiver);
			unregisterReceiver(mHandleSubmitDataReceiver);
			unregisterReceiver(mHandleSubmitDonasiReceiver);
			
			unregisterReceiver(mHandleCekTagihanTelkomReceiver);
			unregisterReceiver(mHandleCekTagihanListrikReceiver);
			unregisterReceiver(mHandleCekTagihanPdamReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.onPause();
	}

	@Override
	protected void onResume() {
		registerReceiver(mHandleLoadListPulsaReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_PULSA"));
		registerReceiver(mHandleLoadListDataReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_DATA"));
        registerReceiver(mHandleLoadListTokenListrikReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_TOKEN_LISTRIK"));
        registerReceiver(mHandleLoadListVoucherGameReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_VOUCHER_GAME"));

        registerReceiver(mHandleLoadListBankReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_BANK"));
        registerReceiver(mHandleLoadListPdamReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_PDAM"));
        registerReceiver(mHandleLoadListGameReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_GAME"));
        
        registerReceiver(mHandleSubmitDepositReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.PROSES_SUBMIT_DEPOSIT"));
		registerReceiver(mHandleSubmitPulsaReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.PROSES_SUBMIT_PULSA"));
		registerReceiver(mHandleSubmitDataReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.PROSES_SUBMIT_DATA"));
		registerReceiver(mHandleSubmitDonasiReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.PROSES_SUBMIT_DONASI"));
		
		registerReceiver(mHandleCekTagihanTelkomReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.PROSES_CEK_TAGIHAN_TELKOM"));
		registerReceiver(mHandleCekTagihanListrikReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.PROSES_CEK_TAGIHAN_LISTRIK"));
		registerReceiver(mHandleCekTagihanPdamReceiver, new IntentFilter("karangtarunaku.application.com.karangtarunaku.PROSES_CEK_TAGIHAN_PDAM"));

		super.onResume();
	}

	private final BroadcastReceiver mHandleCekTagihanPdamReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);
			dialog_loading.dismiss();
			if (success) {

				BayarPdamFragment.edit_customer.setText(tagihan_pdam_selectd.getCustomer());
				BayarPdamFragment.edit_nomer.setText(tagihan_pdam_selectd.getId_pelanggan());
				BayarPdamFragment.edit_periode.setText(tagihan_pdam_selectd.getPeriode());
				BayarPdamFragment.edit_tagihan.setText(CommonUtilities.getCurrencyFormat(tagihan_pdam_selectd.getTagihan(), "Rp. "));
				BayarPdamFragment.edit_denda.setText(CommonUtilities.getCurrencyFormat(tagihan_pdam_selectd.getDenda(), "Rp. "));
				BayarPdamFragment.edit_admin.setText(CommonUtilities.getCurrencyFormat(tagihan_pdam_selectd.getAdmin(), "Rp. "));
				BayarPdamFragment.edit_total.setText(CommonUtilities.getCurrencyFormat(tagihan_pdam_selectd.getTotal(), "Rp. "));

				BayarPdamFragment.linear_detail_tagihan.setVisibility(View.VISIBLE);
				BayarPdamFragment.linear_bayar.setVisibility(View.VISIBLE);
			}

		}
	};
	
	private final BroadcastReceiver mHandleCekTagihanListrikReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);
			dialog_loading.dismiss();
			if (success) {

				BayarListrikFragment.edit_customer.setText(tagihan_listrik_selectd.getCustomer());
				BayarListrikFragment.edit_nomer.setText(tagihan_listrik_selectd.getId_pelanggan());
				BayarListrikFragment.edit_tarif.setText(tagihan_listrik_selectd.getTarif());
				BayarListrikFragment.edit_daya.setText(tagihan_listrik_selectd.getDaya());
				BayarListrikFragment.edit_periode.setText(tagihan_listrik_selectd.getPeriode());
				BayarListrikFragment.edit_tagihan.setText(CommonUtilities.getCurrencyFormat(tagihan_listrik_selectd.getTagihan(), "Rp. "));
				BayarListrikFragment.edit_denda.setText(CommonUtilities.getCurrencyFormat(tagihan_listrik_selectd.getDenda(), "Rp. "));
				BayarListrikFragment.edit_admin.setText(CommonUtilities.getCurrencyFormat(tagihan_listrik_selectd.getAdmin(), "Rp. "));
				BayarListrikFragment.edit_total.setText(CommonUtilities.getCurrencyFormat(tagihan_listrik_selectd.getTotal(), "Rp. "));

				BayarListrikFragment.linear_detail_tagihan.setVisibility(View.VISIBLE);
				BayarListrikFragment.linear_bayar.setVisibility(View.VISIBLE);
			}

		}
	};
    

	private final BroadcastReceiver mHandleCekTagihanTelkomReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);
			dialog_loading.dismiss();
			if (success) {
				/*String customer = intent.getStringExtra("customer");
				String periode = intent.getStringExtra("periode");
				double tagihan = intent.getDoubleExtra("tagihan", 0);
				double admin = intent.getDoubleExtra("admin", 0);
				double total = intent.getDoubleExtra("total", 0);*/

				BayarTelkomFragment.edit_customer.setText(tagihan_telkom_selectd.getCustomer());
				BayarTelkomFragment.edit_nomer.setText(tagihan_telkom_selectd.getNo_telepon());
				BayarTelkomFragment.edit_periode.setText(tagihan_telkom_selectd.getPeriode());
				BayarTelkomFragment.edit_tagihan.setText(CommonUtilities.getCurrencyFormat(tagihan_telkom_selectd.getTagihan(), "Rp. "));
				BayarTelkomFragment.edit_admin.setText(CommonUtilities.getCurrencyFormat(tagihan_telkom_selectd.getAdmin(), "Rp. "));
				BayarTelkomFragment.edit_total.setText(CommonUtilities.getCurrencyFormat(tagihan_telkom_selectd.getTotal(), "Rp. "));

				BayarTelkomFragment.linear_detail_tagihan.setVisibility(View.VISIBLE);
				BayarTelkomFragment.linear_bayar.setVisibility(View.VISIBLE);
			}

		}
	};

    private final BroadcastReceiver mHandleLoadListVoucherGameReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Boolean success = intent.getBooleanExtra("success", false);

            if (success) {
                vouchergameAdapter = new ListVoucherGameAdapter(context, vouchergamelist);
                BeliVoucherGameFragment.listViewVoucher.setAdapter(vouchergameAdapter);
				BeliVoucherGameFragment.linear_voucher.setVisibility(View.VISIBLE);
            }
        }
    };

	private final BroadcastReceiver mHandleLoadListTokenListrikReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);

			if (success) {
				tokenlistrikAdapter = new ListTokenListrikAdapter(context, tokenlistriklist);
				BeliVoucherListrikFragment.listViewVoucher.setAdapter(tokenlistrikAdapter);
			}

		}
	};

	private final BroadcastReceiver mHandleLoadListDataReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);

			if (success) {
				dataAdapter = new ListDataAdapter(context, datalist);
				IsiDataFragment.listViewData.setAdapter(dataAdapter);
				IsiDataFragment.linear_data.setVisibility(View.VISIBLE);
			}

		}
	};

	private final BroadcastReceiver mHandleSubmitDataReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);
			String message = intent.getStringExtra("message");

			dialog_loading.dismiss();
			if (success) {
				ProsesBerhasilFragment.konfirmasi_pembayaran.setVisibility(metode_pembayaran_id==1?View.VISIBLE:View.GONE);
				ProsesBerhasilFragment.keterangan.setText(message);
				gotoPage(4);
			} else {
				openDialogMessage(message, false);
			}

		}
	};
    
	private final BroadcastReceiver mHandleSubmitPulsaReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);
			String message = intent.getStringExtra("message");

			dialog_loading.dismiss();
			if (success) {
				ProsesBerhasilFragment.konfirmasi_pembayaran.setVisibility(metode_pembayaran_id==1?View.VISIBLE:View.GONE);
				ProsesBerhasilFragment.keterangan.setText(message);
				gotoPage(4);
			} else {
				openDialogMessage(message, false);
			}

		}
	};

	private final BroadcastReceiver mHandleSubmitDonasiReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);
			String message = intent.getStringExtra("message");

			dialog_loading.dismiss();
			if (success) {
				ProsesBerhasilFragment.konfirmasi_pembayaran.setVisibility(metode_pembayaran_id==1?View.VISIBLE:View.GONE);
				ProsesBerhasilFragment.keterangan.setText(message);
				gotoPage(4);
			} else {
				openDialogMessage(message, false);
			}

		}
	};

	private final BroadcastReceiver mHandleSubmitDepositReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);
			String message = intent.getStringExtra("message");

			dialog_loading.dismiss();
			if (success) {
				gotoPage(4);
			} else {
				openDialogMessage(message, false);
			}

		}
	};

	private final BroadcastReceiver mHandleLoadListPulsaReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Boolean success = intent.getBooleanExtra("success", false);

			if (success) {
				pulsaAdapter = new ListPulsaAdapter(context, pulsalist);
				IsiPulsaFragment.listViewPulsa.setAdapter(pulsaAdapter);
				IsiPulsaFragment.linear_pulsa.setVisibility(View.VISIBLE);
			}

		}
	};

	public String checkedBeforeCekTagihanListrik() {
		if(BayarListrikFragment.edit_id_pel.getText().toString().length()==0) {
			total_belanja = 0;
			return "ID Pelangaan / No. Meteran harus diisi.";
		}

		return "";
	}

	public String checkedBeforeCekTagihanPdam() {
		if(pdam_selected==null || BayarPdamFragment.edit_jenis.getText().toString().length()==0) {
			total_belanja = 0;
			return "Jenis tagihan harus diisi.";	
		}

		if(BayarPdamFragment.edit_id_pel.getText().toString().length()==0) {
			total_belanja = 0;
			return "ID Pelangaan / No. Rekening harus diisi.";
		}

		return "";
	}

	public String checkedBeforeCekTagihanTelkom() {
		if(BayarTelkomFragment.edit_nohp.getText().toString().length()==0) {
			total_belanja = 0;
			return "No. Telepon harus diisi.";
		}

		return "";
	}
	
	public String checkedBayarListrikBeforeNext() {
		total_belanja = tagihan_listrik_selectd.getTotal();

		if(total_belanja<=0) {
			total_belanja = 0;
			return "Tidak ada total tagihan.";
		}

		return "";
	}

	public String checkedBayarPdamBeforeNext() {
		total_belanja = tagihan_pdam_selectd.getTotal();

		if(total_belanja<=0) {
			total_belanja = 0;
			return "Tidak ada total tagihan.";
		}

		return "";
	}

	public String checkedBayarTelkomBeforeNext() {
		total_belanja = tagihan_telkom_selectd.getTotal();

		if(total_belanja<=0) {
			total_belanja = 0;
			return "Tidak ada total tagihan.";
		}

		return "";
	}

	public String checkedBeliVoucherListrikBeforeNext() {
		if(BeliVoucherListrikFragment.edit_id_pel.getText().toString().length()==0) {
			total_belanja = 0;
			return "ID Pelanggan / No. Meteran harus diisi.";
		}

		return "";
	}

	public String checkedIsiPulsaBeforeNext() {
		if(IsiPulsaFragment.edit_nohp.getText().toString().length()==0) {
			total_belanja = 0;
			return "No. HP harus diisi.";
		}

		return "";
	}
	
	public String checkedIsiDataBeforeNext() {
		if(IsiDataFragment.edit_nohp.getText().toString().length()==0) {
			total_belanja = 0;
			return "No. HP harus diisi.";
		}

		return "";
	}

	public String checkedDonasiBeforeNext() {
		if(DonasiFragment.edit_nominal.getText().toString().length()==0) {
			total_belanja = 0;
			return "Nominal donasi harus diisi.";
		}

		total_belanja = Double.parseDouble(DonasiFragment.edit_nominal.getText().toString());
		if(total_belanja<=0) {
			total_belanja = 0;
			return "Nominal donasi harus diisi.";
		}

		return "";
	}

	public String checkedDepositSaldoBeforeNext() {
		if(DepositSaldoFragment.edit_nominal.getText().toString().length()==0) {
			total_belanja = 0;
			return "Nominal deposit harus diisi.";
		}

		total_belanja = Double.parseDouble(DepositSaldoFragment.edit_nominal.getText().toString());
		if(total_belanja<=0) {
			total_belanja = 0;
			return "Nominal deposit harus diisi.";
		}

		return "";
	}

	public String checkedMetodePembayaranBeforeNext() {
		if(metode_pembayaran_id<=0) {
			metode_pembayaran_id = 0;
			return "Pilih salah bank metode pembayaran.";
		}

		if(metode_pembayaran_id == 1 && (data_bank==null || data_bank.getId()==0)) {
			metode_pembayaran_id = 1;
			return "Pilih salah satu bank pembayaran.";
		}

		return "";
	}

    public void loadDataVoucherGame() {
        new loadDataVoucherGame().execute();
    }

    public class loadDataVoucherGame extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... urls) {

            vouchergamelist = new ArrayList<>();
            String url = CommonUtilities.SERVER_URL + "/store/androidVoucherGameDataStore.php";
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("jenis", game_selected.getKode()));
            JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
            boolean success = false;
            if(json!=null) {
                try {
                    JSONArray topics = json.isNull("topics")?null:json.getJSONArray("topics");
                    for (int i=0; i<topics.length(); i++) {
                        JSONObject rec = topics.getJSONObject(i);

                        String code = rec.isNull("code")?null:rec.getString("code");
                        String nama = rec.isNull("nama")?null:rec.getString("nama");
                        double harga = rec.isNull("harga")?0:rec.getDouble("harga");

                        vouchergamelist.add(new voucher_game(code, nama, harga));
                    }

                    success = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_VOUCHER_GAME");
            i.putExtra("success", success);
            sendBroadcast(i);

            return null;
        }
    }

	public void loadDataTokenListrik() {
		new loadDataTokenListrik().execute();
	}

	public class loadDataTokenListrik extends AsyncTask<String, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(String... urls) {

			tokenlistriklist = new ArrayList<>();
			String url = CommonUtilities.SERVER_URL + "/store/androidTokenListrikDataStore.php";
			List<NameValuePair> params = new ArrayList<>();
			JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
			boolean success = false;
			if(json!=null) {
				try {
					JSONArray pricelist = json.isNull("pricelist")?null:json.getJSONArray("pricelist");
					for (int i=0; i<pricelist.length(); i++) {
						JSONObject rec = pricelist.getJSONObject(i);

						String code = rec.isNull("code")?null:rec.getString("code");
						String nama = rec.isNull("nama")?null:rec.getString("nama");
						double harga = rec.isNull("harga")?0:rec.getDouble("harga");

						tokenlistriklist.add(new token_listrik(code, nama, harga));
					}

					success = true;
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_TOKEN_LISTRIK");
			i.putExtra("success", success);
			sendBroadcast(i);

			return null;
		}
	}

	public void loadDataPulsa(String nohp) {
		new loadDataPulsa(nohp).execute();
	}

	public class loadDataPulsa extends AsyncTask<String, Void, Void> {
		String nohp;

		loadDataPulsa(String nohp) {
			this.nohp = nohp;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			IsiPulsaFragment.linear_pulsa.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(String... urls) {

			pulsalist = new ArrayList<>();
			String url = CommonUtilities.SERVER_URL + "/store/androidPulsaDataStore.php";
			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("nohp", nohp));
			JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
			boolean success = false;
			if(json!=null) {
				try {
					JSONArray topics_induk = json.isNull("topics")?null:json.getJSONArray("topics");
					for (int i=0; i<topics_induk.length(); i++) {
						JSONObject rec = topics_induk.getJSONObject(i);

						int id_pulsa = rec.isNull("id")?0:rec.getInt("id");
						String kode_pulsa = rec.isNull("code")?null:rec.getString("code");
						String nama_pulsa = rec.isNull("description")?null:rec.getString("description");
						String kode_service = rec.isNull("code")?null:rec.getString("code");
						String nama_service = rec.isNull("description")?null:rec.getString("description");
						int nominal = rec.isNull("price")?0:rec.getInt("price");
						String tarif = rec.isNull("tarif")?null:rec.getString("tarif");
						String etd = rec.isNull("etd")?null:rec.getString("etd");
						String gambar = rec.isNull("logo")?null:rec.getString("logo");
						pulsalist.add(new pulsa(id_pulsa, kode_pulsa, nama_pulsa, kode_service, nama_service, nominal, etd, tarif, gambar));
					}

					success = true;
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_PULSA");
			i.putExtra("success", success);
			sendBroadcast(i);

			return null;
		}
	}
	
	public void loadDataData(String nohp) {
		new loadDataData(nohp).execute();
	}

	public class loadDataData extends AsyncTask<String, Void, Void> {
		String nohp;

		loadDataData(String nohp) {
			this.nohp = nohp;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			IsiDataFragment.linear_data.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(String... urls) {

			datalist = new ArrayList<>();
			String url = CommonUtilities.SERVER_URL + "/store/androidDataDataStore.php";
			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("nohp", nohp));
			JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
			boolean success = false;
			if(json!=null) {
				try {
					JSONArray topics_induk = json.isNull("topics")?null:json.getJSONArray("topics");
					for (int i=0; i<topics_induk.length(); i++) {
						JSONObject rec = topics_induk.getJSONObject(i);

						int id_data = rec.isNull("id")?0:rec.getInt("id");
						String kode_data = rec.isNull("code")?null:rec.getString("code");
						String nama_data = rec.isNull("description")?null:rec.getString("description");
						String kode_service = rec.isNull("code")?null:rec.getString("code");
						String nama_service = rec.isNull("description")?null:rec.getString("description");
						int nominal = rec.isNull("price")?0:rec.getInt("price");
						String tarif = rec.isNull("tarif")?null:rec.getString("tarif");
						String etd = rec.isNull("etd")?null:rec.getString("etd");
						String gambar = rec.isNull("logo")?null:rec.getString("logo");
						datalist.add(new pulsa(id_data, kode_data, nama_data, kode_service, nama_service, nominal, etd, tarif, gambar));
					}

					success = true;
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_DATA");
			i.putExtra("success", success);
			sendBroadcast(i);

			return null;
		}
	}

	public class prosesCekTagihanTelkom extends AsyncTask<String, Void, Void> {
		String nohp;

		prosesCekTagihanTelkom(String nohp) {
			this.nohp = nohp;
            tagihan_telkom_selectd = new tagihan_telkom(this.nohp, "","",0,0,0);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			openDialogLoading();
			BayarTelkomFragment.linear_detail_tagihan.setVisibility(View.GONE);
			BayarTelkomFragment.linear_bayar.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(String... urls) {

			datalist = new ArrayList<>();
			String url = CommonUtilities.SERVER_URL + "/store/androidCekTagihanTelkomDataStore.php";
			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("nohp", this.nohp));
			JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
			boolean success = false;
            String number = this.nohp;
			String customer = "";
			String periode = "";
			double tagihan = 0;
			double admin = 0;
			double total = 0;
			if(json!=null) {
				try {
					/*{
					  "response": "Success",
					  "inquiry": {
						"number": "0313282370",
						"customer": "NAMA PELANGGAN",
						"divre": "03",
						"datel": "0002",
						"kode_area": "0031",
						"jml_tagihan": "1",
						"detail_tagihan": [
						  {
							"periode": "NOV 2015",
							"tagihan": "68360",
							"admin": "2500",
							"total": 70860
						  }
						],
						"total_tagihan": 70860,
						"code": "TELKOMPSTN",
						"inquiry_id": "6012776"
					  }
					}*/
					success = (json.isNull("response")?null:json.getString("response")).equalsIgnoreCase("Success");
					JSONObject inquiry = json.isNull("inquiry")?null:json.getJSONObject("inquiry");
					if(inquiry!=null) {
                        number = inquiry.isNull("number")	?null:inquiry.getString("number");
                        customer = inquiry.isNull("customer")	?null:inquiry.getString("customer");
                        JSONArray detail_tagihan = inquiry.isNull("detail_tagihan")?null:inquiry.getJSONArray("detail_tagihan");
						if(detail_tagihan!=null) {
							for (int i=0; i<detail_tagihan.length(); i++) {
								JSONObject rec = detail_tagihan.getJSONObject(i);
								periode = rec.isNull("periode")?null:rec.getString("periode");
								tagihan = rec.isNull("tagihan")?0:rec.getDouble("tagihan");
								admin = rec.isNull("admin")?0:rec.getDouble("admin");
								total = rec.isNull("total")?0:rec.getDouble("total");
								break;
							}
						}
					}





				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

            tagihan_telkom_selectd = new tagihan_telkom(number, customer,periode,tagihan,admin,total);
			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.PROSES_CEK_TAGIHAN_TELKOM");
			i.putExtra("success", success);
			/*i.putExtra("customer", customer);
			i.putExtra("periode", periode);
			i.putExtra("tagihan", tagihan);
			i.putExtra("admin", admin);
			i.putExtra("total", total);*/
			sendBroadcast(i);

			return null;
		}
	}

	public class prosesCekTagihanListrik extends AsyncTask<String, Void, Void> {
		String id_pel;

		prosesCekTagihanListrik(String id_pel) {
			this.id_pel = id_pel;
			tagihan_listrik_selectd = new tagihan_listrik(this.id_pel, "","", "", "",0, 0,0,0);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			openDialogLoading();
			BayarListrikFragment.linear_detail_tagihan.setVisibility(View.GONE);
			BayarListrikFragment.linear_bayar.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(String... urls) {

			datalist = new ArrayList<>();
			String url = CommonUtilities.SERVER_URL + "/store/androidCekTagihanListrikDataStore.php";
			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("id_pel", this.id_pel));
			JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
			boolean success = false;
			String number = this.id_pel;
			String customer = "";
			String tarif = "";
			String daya = "";
			String periode = "";
			double tagihan = 0;
			double denda = 0;
			double admin = 0;
			double total = 0;
			if(json!=null) {
				try {

					success = (json.isNull("response")?null:json.getString("response")).equalsIgnoreCase("Success");
					JSONObject inquiry = json.isNull("inquiry")?null:json.getJSONObject("inquiry");
					if(inquiry!=null) {
						number = inquiry.isNull("number")	?null:inquiry.getString("number");
						customer = inquiry.isNull("customer")	?null:inquiry.getString("customer");
						tarif = inquiry.isNull("tarif")	?null:inquiry.getString("tarif");
						daya = inquiry.isNull("daya")	?null:inquiry.getString("daya");
						JSONArray detail_tagihan = inquiry.isNull("detail_tagihan")?null:inquiry.getJSONArray("detail_tagihan");
						if(detail_tagihan!=null) {
							for (int i=0; i<detail_tagihan.length(); i++) {
								JSONObject rec = detail_tagihan.getJSONObject(i);
								periode = rec.isNull("periode")?null:rec.getString("periode");
								tagihan = rec.isNull("nilaiTagihan")?0:rec.getDouble("nilaiTagihan");
								denda = rec.isNull("denda")?0:rec.getDouble("denda");
								admin = rec.isNull("admin")?0:rec.getDouble("admin");
								total = rec.isNull("total")?0:rec.getDouble("total");
								break;
							}
						}
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			tagihan_listrik_selectd = new tagihan_listrik(number, customer, tarif, daya, periode, tagihan, denda, admin, total);
			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.PROSES_CEK_TAGIHAN_LISTRIK");
			i.putExtra("success", success);
			sendBroadcast(i);

			return null;
		}
	}

	public class prosesCekTagihanPdam extends AsyncTask<String, Void, Void> {
		String id_pel;

		prosesCekTagihanPdam(String id_pel) {
			this.id_pel = id_pel;
			tagihan_pdam_selectd = new tagihan_pdam(this.id_pel, "","", 0, 0,0, 0);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			openDialogLoading();
			BayarPdamFragment.linear_detail_tagihan.setVisibility(View.GONE);
			BayarPdamFragment.linear_bayar.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(String... urls) {

			datalist = new ArrayList<>();
			String url = CommonUtilities.SERVER_URL + "/store/androidCekTagihanPdamDataStore.php";
			List<NameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("id_pel", this.id_pel));
			params.add(new BasicNameValuePair("jenis_tagihan", pdam_selected.getKode()));
			JSONObject json = new JSONParser().getJSONFromUrl(url, params, null);
			boolean success = false;
			String number = this.id_pel;
			String customer = "";
			String periode = "";
			double tagihan = 0;
			double denda = 0;
			double admin = 0;
			double total = 0;
			if(json!=null) {
				try {

					success = (json.isNull("response")?null:json.getString("response")).equalsIgnoreCase("Success");
					JSONObject inquiry = json.isNull("inquiry")?null:json.getJSONObject("inquiry");
					if(inquiry!=null) {
						number = inquiry.isNull("number")	?null:inquiry.getString("number");
						customer = inquiry.isNull("customer")	?null:inquiry.getString("customer");
						JSONArray detail_tagihan = inquiry.isNull("detail_tagihan")?null:inquiry.getJSONArray("detail_tagihan");
						if(detail_tagihan!=null) {
							for (int i=0; i<detail_tagihan.length(); i++) {
								JSONObject rec = detail_tagihan.getJSONObject(i);
								periode = rec.isNull("periode")?null:rec.getString("periode");
								tagihan = rec.isNull("nilaiTagihan")?0:rec.getDouble("nilaiTagihan");
								denda = rec.isNull("denda")?0:rec.getDouble("denda");
								admin = rec.isNull("admin")?0:rec.getDouble("admin");
								total = rec.isNull("total")?0:rec.getDouble("total");
								break;
							}
						}
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			tagihan_pdam_selectd = new tagihan_pdam(number, customer, periode, tagihan, denda, admin, total);
			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.PROSES_CEK_TAGIHAN_PDAM");
			i.putExtra("success", success);
			sendBroadcast(i);

			return null;
		}
	}
	
	public void openDialogLoading() {
		dialog_loading.setCancelable(false);
		dialog_loading.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog_loading.show();
	}

	public void openDialogMessage(String message, boolean status) {
		text_informasi.setText(message);
		text_title.setText(status?"BERHASIL":"KESALAHAN");
		dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog_informasi.show();
	}
	
	public void submitPpob() {
		openDialogLoading();
		switch (ppob_id) {
			case 1:
				new prosesSubmitDeposit().execute();
				return;
			case 2:
				new prosesSubmitPulsa().execute();
				return;
			case 3:
				new prosesSubmitData().execute();
				return;
			case 20:
				new prosesSubmitDonasi().execute();
				return;
		}

		if(ppob_id==1) {


		}
	}

	public class prosesSubmitDeposit extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... urls) {

			JSONParser token_json = new JSONParser();
			JSONObject token = token_json.getJSONFromUrl(CommonUtilities.SERVER_URL + "/store/token.php", null, null);
			String cookies = token_json.getCookies();

			String security_code = "";
			try {
				security_code = token.isNull("security_code") ? "" : token.getString("security_code");
				MCrypt mCrypt = new MCrypt();
				security_code = new String(mCrypt.decrypt(security_code));
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean success = false;
			String message = "";
			if (security_code.length() > 0) {

				String url = CommonUtilities.SERVER_URL + "/store/androidSubmitDeposit.php";
				List<NameValuePair> params = new ArrayList<>();
				params.add(new BasicNameValuePair("user_id", data.getId()+""));
				params.add(new BasicNameValuePair("security_code", security_code));

				//METODE PEMBAYARAN
				params.add(new BasicNameValuePair("metode_pembayaran", metode_pembayaran_id+""));
				params.add(new BasicNameValuePair("bank_id", (metode_pembayaran_id==1?data_bank.getId():"")+""));
				params.add(new BasicNameValuePair("bank_no_rekening", (metode_pembayaran_id==1?data_bank.getNo_rekening():"")));
				params.add(new BasicNameValuePair("bank_nama_pemilik_rekening", (metode_pembayaran_id==1?data_bank.getNama_pemilik_rekening():"")));
				params.add(new BasicNameValuePair("bank_nama", (metode_pembayaran_id==1?data_bank.getNama_bank():"")));
				params.add(new BasicNameValuePair("bank_cabang", (metode_pembayaran_id==1?data_bank.getCabang():"")));
				params.add(new BasicNameValuePair("bank_logo", (metode_pembayaran_id==1?data_bank.getGambar():"")));

				//TOTAL DEPOSIT
				params.add(new BasicNameValuePair("total_deposit", total_belanja+""));

				JSONObject json = new JSONParser().getJSONFromUrl(url, params, cookies);
				if (json != null) {
					try {
						success = json.isNull("success")?false:json.getBoolean("success");
						message = json.isNull("message") ? "" : json.getString("message");
						no_transaksi = json.isNull("no_transaksi")?"":json.getString("no_transaksi");
						total_belanja = json.isNull("jumlah")?0:json.getDouble("jumlah");
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}

			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.PROSES_SUBMIT_DEPOSIT");
			i.putExtra("success", success);
			i.putExtra("message", message);

			sendBroadcast(i);

			return null;
		}
	}

	public class prosesSubmitData extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... urls) {

			JSONParser token_json = new JSONParser();
			JSONObject token = token_json.getJSONFromUrl(CommonUtilities.SERVER_URL + "/store/token.php", null, null);
			String cookies = token_json.getCookies();

			String security_code = "";
			try {
				security_code = token.isNull("security_code") ? "" : token.getString("security_code");
				MCrypt mCrypt = new MCrypt();
				security_code = new String(mCrypt.decrypt(security_code));
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean success = false;
			String message = "";
			if (security_code.length() > 0) {

				String url = CommonUtilities.SERVER_URL + "/store/androidSubmitData.php";
				List<NameValuePair> params = new ArrayList<>();
				params.add(new BasicNameValuePair("user_id", data.getId()+""));
				params.add(new BasicNameValuePair("security_code", security_code));

				//METODE PEMBAYARAN
				params.add(new BasicNameValuePair("metode_pembayaran", metode_pembayaran_id+""));
				params.add(new BasicNameValuePair("bank_id", (metode_pembayaran_id==1?data_bank.getId():"")+""));
				params.add(new BasicNameValuePair("bank_no_rekening", (metode_pembayaran_id==1?data_bank.getNo_rekening():"")));
				params.add(new BasicNameValuePair("bank_nama_pemilik_rekening", (metode_pembayaran_id==1?data_bank.getNama_pemilik_rekening():"")));
				params.add(new BasicNameValuePair("bank_nama", (metode_pembayaran_id==1?data_bank.getNama_bank():"")));
				params.add(new BasicNameValuePair("bank_cabang", (metode_pembayaran_id==1?data_bank.getCabang():"")));
				params.add(new BasicNameValuePair("bank_logo", (metode_pembayaran_id==1?data_bank.getGambar():"")));

				//TOTAL DEPOSIT
				params.add(new BasicNameValuePair("code", produk.getKode_pulsa()));
				params.add(new BasicNameValuePair("description", produk.getNama_pulsa()));
				params.add(new BasicNameValuePair("price", produk.getNominal()+""));
				params.add(new BasicNameValuePair("total", total_belanja+""));

				JSONObject json = new JSONParser().getJSONFromUrl(url, params, cookies);
				if (json != null) {
					try {
						success = json.isNull("success")?false:json.getBoolean("success");
						message = json.isNull("message") ? "" : json.getString("message");
						no_transaksi = json.isNull("no_transaksi")?"":json.getString("no_transaksi");
						total_belanja = json.isNull("jumlah")?0:json.getDouble("jumlah");
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}

			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.PROSES_SUBMIT_DATA");
			i.putExtra("success", success);
			i.putExtra("message", message);

			sendBroadcast(i);

			return null;
		}
	}

	public class prosesSubmitPulsa extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... urls) {

			JSONParser token_json = new JSONParser();
			JSONObject token = token_json.getJSONFromUrl(CommonUtilities.SERVER_URL + "/store/token.php", null, null);
			String cookies = token_json.getCookies();

			String security_code = "";
			try {
				security_code = token.isNull("security_code") ? "" : token.getString("security_code");
				MCrypt mCrypt = new MCrypt();
				security_code = new String(mCrypt.decrypt(security_code));
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean success = false;
			String message = "";
			if (security_code.length() > 0) {

				String url = CommonUtilities.SERVER_URL + "/store/androidSubmitPulsa.php";
				List<NameValuePair> params = new ArrayList<>();
				params.add(new BasicNameValuePair("user_id", data.getId()+""));
				params.add(new BasicNameValuePair("security_code", security_code));

				//METODE PEMBAYARAN
				params.add(new BasicNameValuePair("metode_pembayaran", metode_pembayaran_id+""));
				params.add(new BasicNameValuePair("bank_id", (metode_pembayaran_id==1?data_bank.getId():"")+""));
				params.add(new BasicNameValuePair("bank_no_rekening", (metode_pembayaran_id==1?data_bank.getNo_rekening():"")));
				params.add(new BasicNameValuePair("bank_nama_pemilik_rekening", (metode_pembayaran_id==1?data_bank.getNama_pemilik_rekening():"")));
				params.add(new BasicNameValuePair("bank_nama", (metode_pembayaran_id==1?data_bank.getNama_bank():"")));
				params.add(new BasicNameValuePair("bank_cabang", (metode_pembayaran_id==1?data_bank.getCabang():"")));
				params.add(new BasicNameValuePair("bank_logo", (metode_pembayaran_id==1?data_bank.getGambar():"")));

				//TOTAL DEPOSIT
				params.add(new BasicNameValuePair("code", produk.getKode_pulsa()));
				params.add(new BasicNameValuePair("description", produk.getNama_pulsa()));
				params.add(new BasicNameValuePair("price", produk.getNominal()+""));
				params.add(new BasicNameValuePair("total", total_belanja+""));

				JSONObject json = new JSONParser().getJSONFromUrl(url, params, cookies);
				if (json != null) {
					try {
						success = json.isNull("success")?false:json.getBoolean("success");
						message = json.isNull("message") ? "" : json.getString("message");
						no_transaksi = json.isNull("no_transaksi")?"":json.getString("no_transaksi");
						total_belanja = json.isNull("jumlah")?0:json.getDouble("jumlah");
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}

			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.PROSES_SUBMIT_PULSA");
			i.putExtra("success", success);
			i.putExtra("message", message);

			sendBroadcast(i);

			return null;
		}
	}

	public class prosesSubmitDonasi extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... urls) {

			JSONParser token_json = new JSONParser();
			JSONObject token = token_json.getJSONFromUrl(CommonUtilities.SERVER_URL + "/store/token.php", null, null);
			String cookies = token_json.getCookies();

			String security_code = "";
			try {
				security_code = token.isNull("security_code") ? "" : token.getString("security_code");
				MCrypt mCrypt = new MCrypt();
				security_code = new String(mCrypt.decrypt(security_code));
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean success = false;
			String message = "";
			if (security_code.length() > 0) {

				String url = CommonUtilities.SERVER_URL + "/store/androidSubmitDonasi.php";
				List<NameValuePair> params = new ArrayList<>();
				params.add(new BasicNameValuePair("user_id", data.getId()+""));
				params.add(new BasicNameValuePair("security_code", security_code));

				//METODE PEMBAYARAN
				params.add(new BasicNameValuePair("metode_pembayaran", metode_pembayaran_id+""));
				params.add(new BasicNameValuePair("bank_id", (metode_pembayaran_id==1?data_bank.getId():"")+""));
				params.add(new BasicNameValuePair("bank_no_rekening", (metode_pembayaran_id==1?data_bank.getNo_rekening():"")));
				params.add(new BasicNameValuePair("bank_nama_pemilik_rekening", (metode_pembayaran_id==1?data_bank.getNama_pemilik_rekening():"")));
				params.add(new BasicNameValuePair("bank_nama", (metode_pembayaran_id==1?data_bank.getNama_bank():"")));
				params.add(new BasicNameValuePair("bank_cabang", (metode_pembayaran_id==1?data_bank.getCabang():"")));
				params.add(new BasicNameValuePair("bank_logo", (metode_pembayaran_id==1?data_bank.getGambar():"")));

				//TOTAL DONASI
				params.add(new BasicNameValuePair("total_donasi", total_belanja+""));

				JSONObject json = new JSONParser().getJSONFromUrl(url, params, cookies);
				if (json != null) {
					try {
						success = json.isNull("success")?false:json.getBoolean("success");
						message = json.isNull("message") ? "" : json.getString("message");
						no_transaksi = json.isNull("no_transaksi")?"":json.getString("no_transaksi");
						total_belanja = json.isNull("jumlah")?0:json.getDouble("jumlah");
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}

			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.PROSES_SUBMIT_DONASI");
			i.putExtra("success", success);
			i.putExtra("message", message);

			sendBroadcast(i);

			return null;
		}
	}
	public void loadDataPesanan() {

		if(data_bank!=null) {
			//PEMBAYARAN
			KonfirmasiPemesananFragment.metode_pembayaran.setText(metode_pembayaran_id == 1 ? "Transfer Bank: " : (metode_pembayaran_id == 2 ? "Saldo" : ""));
			KonfirmasiPemesananFragment.image_pembayaran.setVisibility(metode_pembayaran_id == 3 ? View.GONE : View.VISIBLE);
			KonfirmasiPemesananFragment.detail_pembayaran.setVisibility(metode_pembayaran_id == 3 ? View.GONE : View.VISIBLE);
			KonfirmasiPemesananFragment.detail_pembayaran.setText(data_bank.getNama_bank() + "\n" + data_bank.getNo_rekening() + " an. " + data_bank.getNama_pemilik_rekening());
			loadPembayaranLogo(data_bank.getGambar());
		}

			//LIST PESANAN
			pesananAdapter.UpdatePesananAdapter(pesananlist);

			//TOTAL
			loadFieldGrandTotal();
			KonfirmasiPemesananFragment.detail_view.setVisibility(View.VISIBLE);
	}

	private void loadFieldGrandTotal() {
		KonfirmasiPemesananFragment.total.setText(CommonUtilities.getCurrencyFormat(total_belanja, ""));
	}

	public void loadPembayaranLogo(String gambar) {
		String server = CommonUtilities.SERVER_URL;
		String url = server + "/uploads/bank/" + gambar;
		imageLoader.displayImage(url, KonfirmasiPemesananFragment.image_pembayaran, imageOptionPembayaran);

	}
	
	private final BroadcastReceiver mHandleLoadListBankReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Boolean success = intent.getBooleanExtra("success", false);

			selectBankAdapter.UpdateBankAdapter(banklist);
			dialog_loading.dismiss();
			if(success) {
				MetodePembayaranFragment.detail_view.setVisibility(View.VISIBLE);
			} else {
				retry.setVisibility(View.VISIBLE);
			}
		}
	};
	
	public void setInitialMetodePembayaran() {
		MetodePembayaranFragment.linear_metode_saldo.setVisibility(ppob_id==1?View.GONE:View.VISIBLE);
		selectBankAdapter = new SelectBankPpobAdapter(context, banklist);
		MetodePembayaranFragment.listViewBank.setAdapter(selectBankAdapter);

		MetodePembayaranFragment.metode_bayar_transfer_bank.setChecked(metode_pembayaran_id==1);
		MetodePembayaranFragment.metode_bayar_saldo.setChecked(metode_pembayaran_id==2);
	}

	public void setInitialKonfirmasiPesanan() {
		pesananAdapter = new PesananAdapter(context, pesananlist);
		KonfirmasiPemesananFragment.listViewPesanan.setAdapter(pesananAdapter);
	}

    public void loadDataJenisVoucherGame() {
        if(listGame.size()==0) {
            new loadDataGame().execute();
        }
    }
    
	
	public void loadDataPdam() {
		if(listPdam.size()==0) {
			new loadDataPdam().execute();
		}
	}
	
	public void loadDataBank() {
		if(banklist.size()==0) {
			new loadDataBank().execute();
		} else {
			MetodePembayaranFragment.detail_view.setVisibility(View.VISIBLE);
		}
	}

	public int getBank_index() {
		return this.bank_index;
	}

	public void setMetodePembayaran(int metodePembayaran) {
		metode_pembayaran_id = metodePembayaran;
	}



	public void setMetodePembayaranBank(int index, bank databank) {

		bank_index = index;
		data_bank = databank;
	}

	public class loadDataBank extends AsyncTask<String, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			MetodePembayaranFragment.detail_view.setVisibility(View.INVISIBLE);
		}

		@Override
		protected Void doInBackground(String... urls) {

			String url = CommonUtilities.SERVER_URL + "/store/androidBankDataStore.php";
			JSONObject json = new JSONParser().getJSONFromUrl(url, null, null);
			boolean success = false;
			if (json != null) {
				try {
					JSONArray data = json.isNull("topics") ? null : json.getJSONArray("topics");
					for (int i = 0; i < data.length(); i++) {
						JSONObject rec = data.getJSONObject(i);

						int id = rec.isNull("id") ? 0 : rec.getInt("id");
						String no_rekening = rec.isNull("no_rekening") ? "" : rec.getString("no_rekening");
						String nama_pemilik_rekening = rec.isNull("nama_pemilik_rekening") ? "" : rec.getString("nama_pemilik_rekening");
						String nama_bank = rec.isNull("nama_bank") ? "" : rec.getString("nama_bank");
						String cabang = rec.isNull("cabang") ? "" : rec.getString("cabang");
						String gambar = rec.isNull("gambar") ? "" : rec.getString("gambar");

						banklist.add(i, new bank(id, no_rekening, nama_pemilik_rekening, nama_bank, cabang, gambar));
						banklist.get(i).setIs_selected(bank_index==1);
					}
					success = true;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_BANK");
			i.putExtra("success", success);
			sendBroadcast(i);

			return null;
		}
	}

	private final BroadcastReceiver mHandleLoadListPdamReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			if(dialog_loading.isShowing()) {
				loadListArray();
				dialog_loading.dismiss();
				dialog_listview.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				dialog_listview.show();
			}
		}
	};

    private final BroadcastReceiver mHandleLoadListGameReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(dialog_loading.isShowing()) {
                loadListArray();
                dialog_loading.dismiss();
                dialog_listview.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_listview.show();
            }
        }
    };

	public class loadDataGame extends AsyncTask<String, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(String... urls) {

			String url = CommonUtilities.SERVER_URL + "/store/androidGameDataStore.php";
			JSONObject json = new JSONParser().getJSONFromUrl(url, null, null);
			boolean success = false;
			if (json != null) {
				try {
					JSONArray data = json.isNull("topics") ? null : json.getJSONArray("topics");
					for (int i = 0; i < data.length(); i++) {
						JSONObject rec = data.getJSONObject(i);

						int id = rec.isNull("id") ? 0 : rec.getInt("id");
						String kode = rec.isNull("kode") ? "" : rec.getString("kode");
						String nama = rec.isNull("nama") ? "" : rec.getString("nama");

						listGame.add(i, new game(id, kode, nama));
					}
					success = true;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_GAME");
			i.putExtra("success", success);
			sendBroadcast(i);

			return null;
		}
	}

    public class loadDataPdam extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... urls) {

            String url = CommonUtilities.SERVER_URL + "/store/androidPdamDataStore.php";
            JSONObject json = new JSONParser().getJSONFromUrl(url, null, null);
            boolean success = false;
            if (json != null) {
                try {
                    JSONArray data = json.isNull("topics") ? null : json.getJSONArray("topics");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject rec = data.getJSONObject(i);

                        int id = rec.isNull("id") ? 0 : rec.getInt("id");
                        String kode = rec.isNull("kode") ? "" : rec.getString("kode");
                        String nama = rec.isNull("nama") ? "" : rec.getString("nama");

                        listPdam.add(i, new pdam(id, kode, nama));
                    }
                    success = true;
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            Intent i = new Intent("karangtarunaku.application.com.karangtarunaku.LOAD_DATA_PDAM");
            i.putExtra("success", success);
            sendBroadcast(i);

            return null;
        }
    }
    
	public void loadFieldTotalTransfer() {
		
		MetodePembayaranFragment.total_tagihan.setText(CommonUtilities.getCurrencyFormat(total_belanja, "Rp. "));
	}
	
	private void app_close() {
		if(page==1) {
			Intent intent = new Intent();
			setResult(RESULT_OK, intent);
			finish();
		} else if(page==4) {
			open_konfirmasi_page();
		} else {
			gotoPage(page-1);
		}
	}

	public void open_konfirmasi_page() {
		Intent i = new Intent();
		i.putExtra("ppob_id", ppob_id);
		i.putExtra("no_transaksi", no_transaksi);
		i.putExtra("jumlah", total_belanja);
		setResult(RESULT_OK, i);
		finish();
	}
	public void openDialogLoadingEkspedisi() {
		dialog_loading.setCancelable(true);
		dialog_loading.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog_loading.show();
	}

	public void loadDialogListView(String act) {
		action = act;
        if(action.equalsIgnoreCase("pdam") && listPdam.size()==0) {
            openDialogLoadingEkspedisi();
        } else if(action.equalsIgnoreCase("game") && listGame.size()==0) {
            openDialogLoadingEkspedisi();
        } else {
			loadListArray();
			dialog_listview.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog_listview.show();
		}
	}

	public void loadListArray() {
		String[] from = new String[] { getResources().getString(R.string.list_dialog_title) };
		int[] to = new int[] { R.id.txt_title };

		List<HashMap<String, String>> fillMaps = new ArrayList<>();
        if(action.equalsIgnoreCase("pdam")) {
            for (pdam data : listPdam) {
                HashMap<String, String> map = new HashMap<>();
                map.put(getResources().getString(R.string.list_dialog_title), data.getNama());

                fillMaps.add(map);
            }
        } else if(action.equalsIgnoreCase("game")) {
            for (game data : listGame) {
                HashMap<String, String> map = new HashMap<>();
                map.put(getResources().getString(R.string.list_dialog_title), data.getNama());

                fillMaps.add(map);
            }
        }

		SimpleAdapter adapter = new SimpleAdapter(context, fillMaps, R.layout.item_list_dialog, from, to);
		listview.setAdapter(adapter);
	}
}
