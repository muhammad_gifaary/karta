package karangtarunaku.application.com.karangtarunaku;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import customfonts.MyEditText;
import customfonts.MyTextView;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.GalleryFilePath;
import karangtarunaku.application.com.libs.ResizableImageView;
import karangtarunaku.application.com.model.user;

public class AddKartagramActivity extends AppCompatActivity {

    Context context;
    private static Uri mImageCaptureUri;

    final int REQUEST_CODE_FROM_GALLERY = 01;
    final int REQUEST_CODE_FROM_CAMERA  = 02;

    ImageView back;
    MyEditText keterangan;
    MyTextView simpan;

    user data;
    Dialog dialog_loading;
    Dialog dialog_informasi;
    MyTextView btn_ok;
    MyTextView text_title;
    MyTextView text_informasi;
    ResizableImageView gambar;
    MyTextView upload;

    Dialog dialog_pilih_gambar;
    TextView from_camera, from_galery;
    ImageLoader imageLoader;
    DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kartagram);

        context = AddKartagramActivity.this;
        data = CommonUtilities.getSettingUser(context);

        CommonUtilities.initImageLoader(context);
        imageLoader = ImageLoader.getInstance();
        options = CommonUtilities.getOptionsImage(R.drawable.attachment, R.drawable.attachment);

        back = (ImageView) findViewById(R.id.back);
        gambar = (ResizableImageView) findViewById(R.id.gambar);
        upload = (MyTextView) findViewById(R.id.upload);
        keterangan = (MyEditText) findViewById(R.id.edit_keterangan);
        simpan = (MyTextView) findViewById(R.id.simpan);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                selectImage();
            }
        });

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = checkedBeforeSave();
                if(message.length()==0) {
                    new prosesSaveKartagram().execute();
                } else {
                    text_informasi.setText(message);
                    text_title.setText("KESALAHAN");
                    dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog_informasi.show();
                }

            }
        });

        dialog_loading = new Dialog(context);
        dialog_loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_loading.setCancelable(false);
        dialog_loading.setContentView(R.layout.loading_dialog);

        dialog_informasi = new Dialog(context);
        dialog_informasi.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_informasi.setCancelable(true);
        dialog_informasi.setContentView(R.layout.informasi_dialog);

        btn_ok = (MyTextView) dialog_informasi.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog_informasi.dismiss();
            }
        });

        text_title = (MyTextView) dialog_informasi.findViewById(R.id.text_title);
        text_informasi = (MyTextView) dialog_informasi.findViewById(R.id.text_dialog);

        dialog_pilih_gambar = new Dialog(context);
        dialog_pilih_gambar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_pilih_gambar.setCancelable(true);
        dialog_pilih_gambar.setContentView(R.layout.pilih_gambar_dialog);

        from_galery = (TextView) dialog_pilih_gambar.findViewById(R.id.txtFromGalley);
        from_galery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                dialog_pilih_gambar.dismiss();
                fromGallery();
            }
        });

        from_camera = (TextView) dialog_pilih_gambar.findViewById(R.id.txtFromCamera);
        from_camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                dialog_pilih_gambar.dismiss();
                fromCamera();
            }
        });

    }

    private String checkedBeforeSave() {

        if(mImageCaptureUri==null) {
            return "Photo harus harus diupload.";
        }

        return "";
    }

    public void selectImage() {
        dialog_pilih_gambar.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_pilih_gambar.show();
    }

    private void fromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_FROM_GALLERY);
    }

    private void fromCamera() {

        Intent intent = new Intent(context, AmbilFotoActivity.class);
        startActivityForResult(intent, REQUEST_CODE_FROM_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            String fileName = new SimpleDateFormat("yyyyMMddhhmmss'.jpg'").format(new Date());
            String dest = CommonUtilities.getOutputPath(context, "images")+ File.separator+fileName;
            switch(requestCode) {
                case REQUEST_CODE_FROM_CAMERA:
                    CommonUtilities.compressImage(context, data.getStringExtra("path"), dest);
                    mImageCaptureUri = Uri.fromFile(new File(dest));
                    gambar.setImageURI(mImageCaptureUri);

                    break;
                case REQUEST_CODE_FROM_GALLERY:
                    Uri selectedUri = data.getData();
                    CommonUtilities.compressImage(context, GalleryFilePath.getPath(context, selectedUri), dest);
                    mImageCaptureUri = Uri.fromFile(new File(dest));
                    gambar.setImageURI(mImageCaptureUri);
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }


    public void openDialogLoading(boolean cancelable) {
        dialog_loading.setCancelable(cancelable);
        dialog_loading.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_loading.show();
    }



    public class prosesSaveKartagram extends AsyncTask<String, Void, Boolean> {

        int id;
        boolean success;
        String message;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            openDialogLoading(false);
        }

        @Override
        protected Boolean doInBackground(String... urls) {

            success = false;
            message = "Gagal melakukan proses simpan. Silahkan coba lagi!";

            JSONObject result = null;
            try {
                String url = CommonUtilities.SERVER_URL + "/store/androidSaveKartagram.php";
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);

                MultipartEntity reqEntity = new MultipartEntity();
                reqEntity.addPart("users_id", new StringBody(data.getId()+""));
                reqEntity.addPart("keterangan", new StringBody(keterangan.getText().toString()));

                if (mImageCaptureUri != null) {
                    File file = new File(mImageCaptureUri.getPath());
                    if (file.exists()) {
                        FileBody bin_gamber = new FileBody(file);
                        reqEntity.addPart("photo", bin_gamber);
                    }
                }

                httppost.setEntity(reqEntity);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity resEntity = response.getEntity();
                InputStream is = resEntity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                String json = sb.toString();
                System.out.println(json);

                result = new JSONObject(json);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(result!=null) {
                try {
                    id = result.isNull("id")?0:result.getInt("id");
                    success = result.isNull("success")?false:result.getBoolean("success");
                    message = result.isNull("message")?message:result.getString("message");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            return success;
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);

            dialog_loading.dismiss();
            if(success) {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                Intent i = new Intent();
                setResult(RESULT_OK, i);
                finish();
            } else {
                text_informasi.setText(message);
                text_title.setText("KESALAHAN");
                dialog_informasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog_informasi.show();
            }

        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            finish();

            return false;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
