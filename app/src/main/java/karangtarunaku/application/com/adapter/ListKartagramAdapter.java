package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;


import java.util.ArrayList;

import customfonts.MyTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import karangtarunaku.application.com.karangtarunaku.DetailKartagramActivity;
import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.DatabaseHandler;
import karangtarunaku.application.com.libs.SmallBang;
import karangtarunaku.application.com.libs.SmallBangListener;
import karangtarunaku.application.com.libs.SquareImageView;
import karangtarunaku.application.com.model.gallery;

public class ListKartagramAdapter extends BaseAdapter {

	ArrayList<gallery> listDataKartagram = new ArrayList<>();

	public ListKartagramAdapter(ArrayList<gallery> listDataKartagram) {
		this.listDataKartagram = listDataKartagram;
	}

	public void UpdateKartagramAdapter(ArrayList<gallery> listDataKartagram) {
		this.listDataKartagram = listDataKartagram;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return listDataKartagram.size();
	}

	@Override
	public Object getItem(int position) {
		return listDataKartagram.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		public CircleImageView photo_profile;
		public MyTextView username;
		public MyTextView tambah;
		public SquareImageView image;
		public LinearLayout linearLike;
		public ImageView fav1;
		public ImageView fav2;
		public SmallBang mSmallBang;
		public MyTextView likes;	
		public MyTextView caption;

		public int position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {

		final ViewHolder view;
		LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
		if(convertView==null) {
			view = new ViewHolder();
			convertView = inflator.inflate(R.layout.kartagram_item_list, null);

			view.photo_profile  = (CircleImageView) convertView.findViewById(R.id.profile_photo);
			view.username       = (MyTextView) convertView.findViewById(R.id.username);
			view.tambah         = (MyTextView) convertView.findViewById(R.id.tambah);
			view.image          = (SquareImageView) convertView.findViewById(R.id.post_image);
			view.linearLike     = (LinearLayout) convertView.findViewById(R.id.linear_like);
			view.fav1           = (ImageView) convertView.findViewById(R.id.fav1);
			view.fav2           = (ImageView) convertView.findViewById(R.id.fav2);
			view.likes          = (MyTextView) convertView.findViewById(R.id.likes);
			view.caption        = (MyTextView) convertView.findViewById(R.id.caption);

			convertView.setTag(view);
		} else {
			view = (ViewHolder) convertView.getTag();
		}

		final gallery info = listDataKartagram.get(position);
		view.position = listDataKartagram.indexOf(info);

		//view.linearLike.setVisibility(MainActivity.data.getId()>0?View.VISIBLE:View.GONE);
		//view.tambah.setVisibility(MainActivity.data.getId()>0?View.VISIBLE:View.GONE);
		String server = CommonUtilities.SERVER_URL;
		String url = server+"/store/centercrop_3.php?url="+ server+"/uploads/kartagram/"+info.getGambar()+"&width=300&height=300";
		String url_user = server+"/store/centercrop_3.php?url="+ server+"/uploads/member/"+info.getPhoto()+"&width=300&height=300";
		//String url = server+"/uploads/kartagram/"+info.getGambar();
		MainActivity.imageLoader.displayImage(url, view.image, MainActivity.imageOptionKategori);
		MainActivity.imageLoader.displayImage(url_user, view.photo_profile, MainActivity.imageOptionsUser);
		view.username.setText(info.getOwner());
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View convertView) {
				Intent i = new Intent(parent.getContext(), DetailKartagramActivity.class);
				i.putExtra("kartagram", info);
				parent.getContext().startActivity(i);
			}
		});
		view.caption.setText(info.getKeterangan());
		view.likes.setText(info.getLike()>0?info.getLike()+" Suka":"");

		view.mSmallBang = SmallBang.attach2Window((MainActivity) parent.getContext());

		view.fav1.setVisibility(info.getFavorite()?View.GONE:View.VISIBLE);
		view.fav2.setVisibility(info.getFavorite()?View.VISIBLE:View.GONE);

		view.tambah.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                if(MainActivity.data.getId()==0) {
                    ((MainActivity)	parent.getContext()).openErrorLogin();
                } else {
                    ((MainActivity) parent.getContext()).addNewKartagram();
                }
			}
		});

		view.fav1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                if(MainActivity.data.getId()==0) {
                    ((MainActivity)	parent.getContext()).openErrorLogin();
                } else {
                    info.setFavorite(true);
					((MainActivity) parent.getContext()).likeunlikeKartagram(1, info.getId());

                    view.fav2.setVisibility(View.VISIBLE);
                    view.fav1.setVisibility(View.GONE);
                    like(v);
                }

			}

			public void like(View v){
				view.fav2.setImageResource(R.drawable.f4);
				view.mSmallBang.bang(v);
				view.mSmallBang.setmListener(new SmallBangListener() {
					@Override
					public void onAnimationStart() {

					}

					@Override
					public void onAnimationEnd() {

					}


				});
			}
		});

		view.fav2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                if(MainActivity.data.getId()==0) {
                    ((MainActivity)	parent.getContext()).openErrorLogin();
                } else {

                    info.setFavorite(false);
					((MainActivity) parent.getContext()).likeunlikeKartagram(0, info.getId());

                    view.fav2.setVisibility(View.GONE);
                    view.fav1.setVisibility(View.VISIBLE);
                }
			}
		});
		
		return convertView;
	}
}
