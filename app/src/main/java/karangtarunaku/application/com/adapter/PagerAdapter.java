package karangtarunaku.application.com.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import karangtarunaku.application.com.fragment.DaftarPesananBatalFragment;
import karangtarunaku.application.com.fragment.DaftarPesananBelumBayarFragment;
import karangtarunaku.application.com.fragment.DaftarPesananSedangKirimFragment;
import karangtarunaku.application.com.fragment.DaftarPesananSedangProsesFragment;
import karangtarunaku.application.com.fragment.DaftarPesananSelesaiFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new DaftarPesananBelumBayarFragment();
            case 1:
                return new DaftarPesananSedangProsesFragment();
            case 2:
                return new DaftarPesananSedangKirimFragment();
            case 3:
                return new DaftarPesananSelesaiFragment();
            case 4:
                return new DaftarPesananBatalFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}