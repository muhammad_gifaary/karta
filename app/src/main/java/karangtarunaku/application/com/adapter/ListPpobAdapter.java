package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.ResizableImageView;
import karangtarunaku.application.com.model.kategori;

public class ListPpobAdapter extends BaseAdapter {

    ArrayList<kategori> listDataPpob = new ArrayList<>();

    public ListPpobAdapter(ArrayList<kategori> listDataPpob) {
        this.listDataPpob = listDataPpob;
    }

    public void UpdateListPpobAdapter(ArrayList<kategori> listDataPpob) {
        this.listDataPpob = listDataPpob;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listDataPpob.size();
    }

    @Override
    public Object getItem(int position) {
        return listDataPpob.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public ResizableImageView image;
        public MyTextView title;
        public int position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final ViewHolder view;
        LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
        if(convertView==null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.listppob, null);

            view.image = (ResizableImageView) convertView.findViewById(R.id.image);
            view.title = (MyTextView) convertView.findViewById(R.id.title);
            view.image.setLayerType(View.LAYER_TYPE_HARDWARE, null);

            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }

        final kategori prod = listDataPpob.get(position);
        view.position = listDataPpob.indexOf(prod);

        String server = CommonUtilities.SERVER_URL;
        String url = server+"/uploads/ppob/"+listDataPpob.get(position).getHeader();
        MainActivity.imageLoader.displayImage(url, view.image, MainActivity.imageOptionKategori);

        view.title.setText(listDataPpob.get(position).getNama());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                ((MainActivity) parent.getContext()).openPpob(prod);
            }
        });

        return convertView;
    }
}
