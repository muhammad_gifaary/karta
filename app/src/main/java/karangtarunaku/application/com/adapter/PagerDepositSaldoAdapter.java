package karangtarunaku.application.com.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import karangtarunaku.application.com.fragment.DepositSaldoFragment;
import karangtarunaku.application.com.fragment.IsiPulsaFragment;
import karangtarunaku.application.com.fragment.KonfirmasiPemesananFragment;
import karangtarunaku.application.com.fragment.MetodePembayaranFragment;

public class PagerDepositSaldoAdapter extends FragmentStatePagerAdapter {

    public PagerDepositSaldoAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                DepositSaldoFragment depositSaldoFragment = new DepositSaldoFragment();

                return depositSaldoFragment;

            case 1:
                MetodePembayaranFragment metodePembayaranFragment = new MetodePembayaranFragment();

                return metodePembayaranFragment;

            case 2:
                KonfirmasiPemesananFragment konfirmasiPemesananFragment = new KonfirmasiPemesananFragment();

                return konfirmasiPemesananFragment;

            default:
                return null;

        }


    }

    @Override
    public int getCount() {
        return 3;
    }
}