package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.ResizableImageView;
import karangtarunaku.application.com.model.kategori;

public class ListUnitTeknisAdapter extends BaseAdapter {

    ArrayList<kategori> listDataUnitTeknis = new ArrayList<>();

    public ListUnitTeknisAdapter(ArrayList<kategori> listDataUnitTeknis) {
        this.listDataUnitTeknis = listDataUnitTeknis;
    }

    public void UpdateListUnitTeknisAdapter(ArrayList<kategori> listDataUnitTeknis) {
        this.listDataUnitTeknis = listDataUnitTeknis;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listDataUnitTeknis.size();
    }

    @Override
    public Object getItem(int position) {
        return listDataUnitTeknis.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public ResizableImageView image;
        //public MyTextView title;
        public int position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final ViewHolder view;
        LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
        if(convertView==null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.listunitteknis, null);

            view.image = (ResizableImageView) convertView.findViewById(R.id.image);
            //view.title = (MyTextView) convertView.findViewById(R.id.title);
            //view.image.setLayerType(View.LAYER_TYPE_HARDWARE, null);

            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }

        final kategori prod = listDataUnitTeknis.get(position);
        view.position = listDataUnitTeknis.indexOf(prod);

        String server = CommonUtilities.SERVER_URL;
        String url = server+"/uploads/unitteknis/"+listDataUnitTeknis.get(position).getHeader();
        MainActivity.imageLoader.displayImage(url, view.image, MainActivity.imageOptionKategori);

        //view.title.setText(listDataUnitTeknis.get(position).getNama());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                ((MainActivity) parent.getContext()).openDetailUnitTeknis(prod);
            }
        });

        return convertView;
    }
}
