package karangtarunaku.application.com.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import karangtarunaku.application.com.fragment.BayarListrikFragment;
import karangtarunaku.application.com.fragment.BayarPdamFragment;
import karangtarunaku.application.com.fragment.BayarTelkomFragment;
import karangtarunaku.application.com.fragment.BeliVoucherGameFragment;
import karangtarunaku.application.com.fragment.BeliVoucherListrikFragment;
import karangtarunaku.application.com.fragment.DepositSaldoFragment;
import karangtarunaku.application.com.fragment.DonasiFragment;
import karangtarunaku.application.com.fragment.IsiDataFragment;
import karangtarunaku.application.com.fragment.IsiPulsaFragment;
import karangtarunaku.application.com.fragment.KonfirmasiPemesananFragment;
import karangtarunaku.application.com.fragment.MetodePembayaranFragment;
import karangtarunaku.application.com.fragment.ProsesBerhasilFragment;

public class PagerPpobAdapter extends FragmentStatePagerAdapter {

    int ppob_id;
    public PagerPpobAdapter(int ppob_id, FragmentManager fm) {
        super(fm);
        this.ppob_id = ppob_id;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                switch (ppob_id) {
                    case 1:
                        DepositSaldoFragment depositSaldoFragment = new DepositSaldoFragment();
                        return depositSaldoFragment;

                    case 2:
                        IsiPulsaFragment isiPulsaFragment = new IsiPulsaFragment();
                        return isiPulsaFragment;

                    case 3:
                        IsiDataFragment isiDataFragment = new IsiDataFragment();
                        return isiDataFragment;

                    case 4:
                        BayarTelkomFragment bayarTelkomFragment = new BayarTelkomFragment();
                        return bayarTelkomFragment;

                    case 5:
                        BeliVoucherListrikFragment beliVoucherListrikFragment = new BeliVoucherListrikFragment();
                        return beliVoucherListrikFragment;

                    case 6:
                        BayarListrikFragment bayarListrikFragment = new BayarListrikFragment();
                        return bayarListrikFragment;

                    case 7:
                        BayarPdamFragment bayarPdamFragment = new BayarPdamFragment();
                        return bayarPdamFragment;

                    case 8:
                        BeliVoucherGameFragment beliVoucherGameFragment = new BeliVoucherGameFragment();
                        return beliVoucherGameFragment;

                    case 20:
                        DonasiFragment donasiFragment = new DonasiFragment();
                        return donasiFragment;
                }

            case 1:
                MetodePembayaranFragment metodePembayaranFragment = new MetodePembayaranFragment();

                return metodePembayaranFragment;

            case 2:
                KonfirmasiPemesananFragment konfirmasiPemesananFragment = new KonfirmasiPemesananFragment();

                return konfirmasiPemesananFragment;

            case 3:
                ProsesBerhasilFragment prosesBerhasilFragment = new ProsesBerhasilFragment();

                return prosesBerhasilFragment;

            default:
                return null;

        }


    }

    @Override
    public int getCount() {
        return 4;
    }
}