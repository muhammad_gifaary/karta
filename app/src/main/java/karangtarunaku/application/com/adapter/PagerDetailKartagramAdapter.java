package karangtarunaku.application.com.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import karangtarunaku.application.com.fragment.DetailKartagramFragment;
import karangtarunaku.application.com.fragment.KomentarKartagramFragment;

public class PagerDetailKartagramAdapter extends FragmentStatePagerAdapter {

    Context context;

    public PagerDetailKartagramAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new DetailKartagramFragment();
            case 1:
                return new KomentarKartagramFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}