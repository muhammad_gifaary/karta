package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.ResizableImageView;
import karangtarunaku.application.com.model.news;

public class ListKartanewsAdapter extends BaseAdapter {

	ArrayList<news> listDatanews = new ArrayList<>();

	public ListKartanewsAdapter(ArrayList<news> listDatanews) {
		this.listDatanews = listDatanews;
	}

	public void UpdateKartanewsAdapter(ArrayList<news> listDatanews) {
		this.listDatanews = listDatanews;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return listDatanews.size();
	}

	@Override
	public Object getItem(int position) {
		return listDatanews.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		public ResizableImageView image;
		public MyTextView title;
		public MyTextView kategori;
		public MyTextView tanggal;

		public int position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {

		final ViewHolder view;
		LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
		if(convertView==null) {
			view = new ViewHolder();
			convertView = inflator.inflate(R.layout.news_item_list, null);

			view.image    = (ResizableImageView) convertView.findViewById(R.id.image);
			view.title    = (MyTextView) convertView.findViewById(R.id.judul);
			view.tanggal  = (MyTextView) convertView.findViewById(R.id.tanggal);
			view.kategori = (MyTextView) convertView.findViewById(R.id.kategori);

			convertView.setTag(view);
		} else {
			view = (ViewHolder) convertView.getTag();
		}

		final news info = listDatanews.get(position);
		view.position = listDatanews.indexOf(info);

		String server = CommonUtilities.SERVER_URL;
		String url = server+"/store/centercrop_3.php?url="+ server+"/uploads/kartanews/"+info.getGambar()+"&width=300&height=300";
		//String url = server+"/uploads/kartanews/"+info.getGambar();
		MainActivity.imageLoader.displayImage(url, view.image, MainActivity.imageOptionKartanews);

		view.title.setText(info.getJudul().toUpperCase());
		view.kategori.setText(info.getKategori().toUpperCase());
		view.tanggal.setText(info.getSejak().toUpperCase());

		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View convertView) {
				((MainActivity) parent.getContext()).openDetailKartanews(info);
			}
		});

		return convertView;
	}
}
