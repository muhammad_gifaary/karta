package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.model.voucher_game;

public class ListVoucherGameAdapter extends BaseAdapter {

    ArrayList<voucher_game> listDataVoucherGame = new ArrayList<>();
    Context context;

    public ListVoucherGameAdapter(Context context, ArrayList<voucher_game> listDataVoucherGame) {
        this.context = context;
        this.listDataVoucherGame = listDataVoucherGame;
    }

    public void UpdateListVoucherGameAdapter(ArrayList<voucher_game> listDataVoucherGame) {
        this.listDataVoucherGame = listDataVoucherGame;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listDataVoucherGame.size();
    }

    @Override
    public Object getItem(int position) {
        return listDataVoucherGame.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public MyTextView title;
        public MyTextView beli_produk;
        public int position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final ViewHolder view;
        LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
        if(convertView==null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.voucher_game_item_list, null);

            view.title = (MyTextView) convertView.findViewById(R.id.title);
            view.beli_produk = (MyTextView) convertView.findViewById(R.id.add_to_cart);
            
            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }

        final voucher_game prod = listDataVoucherGame.get(position);
        view.position = listDataVoucherGame.indexOf(prod);


        view.title.setText(listDataVoucherGame.get(position).getNama()+"\n"+CommonUtilities.getCurrencyFormat(listDataVoucherGame.get(position).getHarga(), "Rp. "));

        view.beli_produk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                ((ProsesPpobActivity) parent.getContext()).beliVoucherGame(prod);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {

            }
        });

        return convertView;
    }
}
