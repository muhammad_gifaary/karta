package karangtarunaku.application.com.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import karangtarunaku.application.com.fragment.TabKartaCareFragment;
import karangtarunaku.application.com.fragment.TabKartagramFragment;
import karangtarunaku.application.com.fragment.TabKartanewsFragment;
import karangtarunaku.application.com.fragment.TabKartapayshopFragment;
import karangtarunaku.application.com.fragment.TabKartapediaFragment;

public class PagerMenuAdapter extends FragmentStatePagerAdapter {

    public PagerMenuAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TabKartapediaFragment tab_kartapedia = new TabKartapediaFragment();

                return tab_kartapedia;

            case 1:
                TabKartapayshopFragment tab_ppob = new TabKartapayshopFragment();

                return tab_ppob;

            case 2:
                TabKartanewsFragment tab_kartanews = new TabKartanewsFragment();

                return tab_kartanews;

            case 3:
                TabKartaCareFragment tab_kartagram = new TabKartaCareFragment();

                return tab_kartagram;

            default:
                return null;

        }


    }

    @Override
    public int getCount() {
        return 4;
    }
}