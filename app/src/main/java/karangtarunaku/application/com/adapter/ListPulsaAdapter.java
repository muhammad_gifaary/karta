package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.model.pulsa;

public class ListPulsaAdapter extends BaseAdapter {

    ArrayList<pulsa> listDataPulsa = new ArrayList<>();
    Context context;

    public ListPulsaAdapter(Context context, ArrayList<pulsa> listDataPulsa) {
        this.context = context;
        this.listDataPulsa = listDataPulsa;
    }

    public void UpdateListPulsaAdapter(ArrayList<pulsa> listDataPulsa) {
        this.listDataPulsa = listDataPulsa;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listDataPulsa.size();
    }

    @Override
    public Object getItem(int position) {
        return listDataPulsa.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public ImageView image;
        public MyTextView title;
        public MyTextView beli_produk;
        public int position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final ViewHolder view;
        LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
        if(convertView==null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.pulsa_item_list, null);

            view.image = (ImageView) convertView.findViewById(R.id.image);
            view.title = (MyTextView) convertView.findViewById(R.id.title);
            view.beli_produk = (MyTextView) convertView.findViewById(R.id.add_to_cart);
            
            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }

        final pulsa prod = listDataPulsa.get(position);
        view.position = listDataPulsa.indexOf(prod);

        String server = CommonUtilities.SERVER_URL;
        String url = server+"/uploads/provider/"+listDataPulsa.get(position).getGambar();
        ProsesPpobActivity.imageLoader.displayImage(url, view.image, ProsesPpobActivity.imageOptionPulsa);

        view.title.setText(listDataPulsa.get(position).getNama_service()+"\n"+listDataPulsa.get(position).getTarif());

        view.beli_produk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                ((ProsesPpobActivity) parent.getContext()).beliPulsa(prod);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {

            }
        });

        return convertView;
    }
}
