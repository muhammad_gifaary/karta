package karangtarunaku.application.com.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.model.kategori;

public class KategoriAdapter extends BaseAdapter {

    Context context;
    ArrayList<kategori> kategorilist;

    public KategoriAdapter(Context context, ArrayList<kategori> listKategori) {
        this.context = context;
        this.kategorilist = listKategori;
    }

    public void UpdateShortcutAdapter(ArrayList<kategori> listShortcut) {
        this.kategorilist = listShortcut;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return kategorilist.size();
    }

    @Override
    public Object getItem(int position) {
        return kategorilist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.listkartapedia, null);

            viewHolder = new ViewHolder();

            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.title = (MyTextView) convertView.findViewById(R.id.title);
            viewHolder.image.setLayerType(View.LAYER_TYPE_HARDWARE, null);

            convertView.setTag(viewHolder);
        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        //final kategori data = (kategori) getItem(position);
        viewHolder.position = position;

        String server = CommonUtilities.SERVER_URL;
        String url = server+"/uploads/kartapedia/"+kategorilist.get(position).getHeader();
        MainActivity.imageLoader.displayImage(url, viewHolder.image, MainActivity.imageOptionKartapedia);
        viewHolder.title.setText(kategorilist.get(position).getNama());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                ((MainActivity) context).openDetailKartapedia(kategorilist.get(viewHolder.position));
            }
        });

        return convertView;

    }

    private class ViewHolder {

        ImageView image;
        MyTextView title;
        int position;
    }
}
