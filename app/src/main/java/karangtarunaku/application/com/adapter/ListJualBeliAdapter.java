package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.alexzh.circleimageview.CircleImageView;

import java.util.ArrayList;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.ResizableImageView;
import karangtarunaku.application.com.model.kategori;

public class ListJualBeliAdapter extends BaseAdapter {

    Context context;
    ArrayList<kategori> listDataBelanja = new ArrayList<>();

    public ListJualBeliAdapter(Context context, ArrayList<kategori> listDataBelanja) {
        this.context = context;
        this.listDataBelanja = listDataBelanja;
    }

    public void UpdateListBelanjaAdapter(ArrayList<kategori> listDataBelanja) {
        this.listDataBelanja = listDataBelanja;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listDataBelanja.size();
    }

    @Override
    public Object getItem(int position) {
        return listDataBelanja.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public ResizableImageView image;
        //public MyTextView title;
        public int position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final ViewHolder view;
        LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
        if(convertView==null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.listbelanja, null);

            view.image = (ResizableImageView) convertView.findViewById(R.id.image);
            //view.title = (MyTextView) convertView.findViewById(R.id.title);
            //view.image.setLayerType(View.LAYER_TYPE_HARDWARE, null);

            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }

        final kategori prod = listDataBelanja.get(position);
        view.position = listDataBelanja.indexOf(prod);

        String server = CommonUtilities.SERVER_URL;
        String url = server+"/uploads/category/"+listDataBelanja.get(position).getHeader();
        MainActivity.imageLoader.displayImage(url, view.image, MainActivity.imageOptionKategori);

        //view.title.setText(listDataBelanja.get(position).getNama());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                ((MainActivity) context).openProdukKategori(prod);
            }
        });

        return convertView;
    }
}
