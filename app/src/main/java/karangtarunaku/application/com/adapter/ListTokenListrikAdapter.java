package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.model.token_listrik;

public class ListTokenListrikAdapter extends BaseAdapter {

    ArrayList<token_listrik> listDataTokenListrik = new ArrayList<>();
    Context context;

    public ListTokenListrikAdapter(Context context, ArrayList<token_listrik> listDataTokenListrik) {
        this.context = context;
        this.listDataTokenListrik = listDataTokenListrik;
    }

    public void UpdateListTokenListrikAdapter(ArrayList<token_listrik> listDataTokenListrik) {
        this.listDataTokenListrik = listDataTokenListrik;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listDataTokenListrik.size();
    }

    @Override
    public Object getItem(int position) {
        return listDataTokenListrik.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public MyTextView title;
        public MyTextView beli_produk;
        public int position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final ViewHolder view;
        LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
        if(convertView==null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.token_listrik_item_list, null);

            view.title = (MyTextView) convertView.findViewById(R.id.title);
            view.beli_produk = (MyTextView) convertView.findViewById(R.id.add_to_cart);
            
            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }

        final token_listrik prod = listDataTokenListrik.get(position);
        view.position = listDataTokenListrik.indexOf(prod);


        view.title.setText(listDataTokenListrik.get(position).getNama()+"\n"+CommonUtilities.getCurrencyFormat(listDataTokenListrik.get(position).getHarga(), "Rp. "));

        view.beli_produk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                ((ProsesPpobActivity) parent.getContext()).beliTokenListrik(prod);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {

            }
        });

        return convertView;
    }
}
