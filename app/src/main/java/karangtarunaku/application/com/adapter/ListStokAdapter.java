package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import customfonts.MyEditText;
import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.DetailProdukActivity;
import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.ResizableImageView;
import karangtarunaku.application.com.model.stok;

public class ListStokAdapter extends BaseAdapter {

	ArrayList<stok> liststok = new ArrayList<>();
	Context context;

	public ListStokAdapter(Context context, ArrayList<stok> liststok) {
		this.context = context;
		this.liststok = liststok;
	}

	public void UpdateListStokAdapter(ArrayList<stok> liststok) {
		this.liststok = liststok;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return liststok.size();
	}

	@Override
	public Object getItem(int position) {
		return liststok.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder {
		public MyTextView ukuran;
		public MyTextView warna;
		public MyTextView stok;
		public MyTextView harga;
		public MyEditText qty;
		public int position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {

		final ViewHolder view;
		LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
		if(convertView==null) {
			view = new ViewHolder();
			convertView = inflator.inflate(R.layout.stok_item_list, null);

			view.ukuran = (MyTextView) convertView.findViewById(R.id.ukuran);
			view.warna = (MyTextView) convertView.findViewById(R.id.warna);
			view.harga = (MyTextView) convertView.findViewById(R.id.harga);
			view.stok = (MyTextView) convertView.findViewById(R.id.stok);
			view.qty = (MyEditText) convertView.findViewById(R.id.qty);

			convertView.setTag(view);
		} else {
			view = (ViewHolder) convertView.getTag();
		}

		final stok data = liststok.get(position);
		view.ukuran.setText(data.getUkuran());
		view.warna.setText(data.getWarna());
		view.harga.setText(data.getHarga());
		view.stok.setText((DetailProdukActivity.stok_status==0?data.getQty():((data.getQty()>=DetailProdukActivity.parameter_status?"Ready":(data.getQty()<=DetailProdukActivity.parameter_status?"Limited":"Habis"))))+""); //CommonUtilities.getNumberFormat(data.getQty())+" Pcs");

		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View convertView) {

			}
		});

		return convertView;
	}
}
