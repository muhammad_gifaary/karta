package karangtarunaku.application.com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.model.pulsa;

public class ListDataAdapter extends BaseAdapter {

    ArrayList<pulsa> listDataData = new ArrayList<>();
    Context context;

    public ListDataAdapter(Context context, ArrayList<pulsa> listDataData) {
        this.context = context;
        this.listDataData = listDataData;
    }

    public void UpdateListDataAdapter(ArrayList<pulsa> listDataData) {
        this.listDataData = listDataData;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listDataData.size();
    }

    @Override
    public Object getItem(int position) {
        return listDataData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public ImageView image;
        public MyTextView title;
        public MyTextView beli_produk;
        public int position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final ViewHolder view;
        LayoutInflater inflator =  LayoutInflater.from(parent.getContext());
        if(convertView==null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.pulsa_item_list, null);

            view.image = (ImageView) convertView.findViewById(R.id.image);
            view.title = (MyTextView) convertView.findViewById(R.id.title);
            view.beli_produk = (MyTextView) convertView.findViewById(R.id.add_to_cart);
            
            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }

        final pulsa prod = listDataData.get(position);
        view.position = listDataData.indexOf(prod);

        String server = CommonUtilities.SERVER_URL;
        String url = server+"/uploads/provider/"+listDataData.get(position).getGambar();
        ProsesPpobActivity.imageLoader.displayImage(url, view.image, ProsesPpobActivity.imageOptionPulsa);

        view.title.setText(listDataData.get(position).getNama_service()+"\n"+listDataData.get(position).getTarif());

        view.beli_produk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                ((ProsesPpobActivity) parent.getContext()).beliData(prod);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {

            }
        });

        return convertView;
    }
}
