package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.EndlessScrollListener;
import karangtarunaku.application.com.libs.ExpandableHeightListView;

public class TabKartagramFragment extends Fragment {

    ListView listview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_tab_kartagram, container, false);

        listview = (ListView) rootView.findViewById(R.id.listview);
        listview.setAdapter(MainActivity.kartagramAdapter);

        listview.setOnScrollListener(new EndlessScrollListener() {

            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {

                ((MainActivity) getActivity()).loadDataKartagram(false);

                return true;
            }
        });

        return rootView;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
