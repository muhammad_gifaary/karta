package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import customfonts.MyEditText;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.ExpandableHeightListView;
import karangtarunaku.application.com.libs.HeaderGridView;

public class BeliVoucherListrikFragment extends Fragment {


    public static MyEditText edit_id_pel;
    public static ExpandableHeightListView listViewVoucher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_beli_voucher_listrik, container, false);

        edit_id_pel = (MyEditText) rootView.findViewById(R.id.edit_id_pel);
        listViewVoucher = (ExpandableHeightListView) rootView.findViewById(R.id.lisview);

        ((ProsesPpobActivity) getActivity()).loadDataTokenListrik();

		return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
