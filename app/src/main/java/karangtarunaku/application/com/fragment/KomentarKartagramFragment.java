package karangtarunaku.application.com.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import customfonts.MyEditText;
import karangtarunaku.application.com.karangtarunaku.DetailKartagramActivity;
import karangtarunaku.application.com.karangtarunaku.R;

public class KomentarKartagramFragment extends Fragment {

	public static LinearLayout llMsgCompose;
	public static ListView listViewMessage;
	public static MyEditText inputMsg;
	public static ImageView btn_send;
	public static ProgressBar progres_save;

	public static ProgressBar loading;
	public static LinearLayout retry;
	public static Button btnReload;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.fragment_komentar_kartagram, container, false);

		llMsgCompose = (LinearLayout) rootView.findViewById(R.id.llMsgCompose);
		listViewMessage = (ListView) rootView.findViewById(R.id.listMessage);
		inputMsg = (MyEditText) rootView.findViewById(R.id.inputMsg);
		btn_send = (ImageView) rootView.findViewById(R.id.btnSend);
		progres_save = (ProgressBar) rootView.findViewById(R.id.progres_save);

		loading = (ProgressBar) rootView.findViewById(R.id.pgbarLoading);
		retry = (LinearLayout) rootView.findViewById(R.id.loadMask);
		btnReload = (Button) rootView.findViewById(R.id.btnReload);
		btnReload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((DetailKartagramActivity) getActivity()).loadDataMessage();
			}
		});


		inputMsg.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEND) {
					((DetailKartagramActivity) getActivity()).addKomentar();
					return true;
				}
				return false;
			}
		});

		btn_send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				((DetailKartagramActivity) getActivity()).addKomentar();
			}
		});

		listViewMessage.setAdapter(DetailKartagramActivity.messageAdapter);


		((DetailKartagramActivity) getActivity()).loadDataMessage();

		return rootView;
	}

}
