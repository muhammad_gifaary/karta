package karangtarunaku.application.com.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import karangtarunaku.application.com.adapter.PagerMenuAdapter;
import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.CustomTabLayout;

public class DashboardFragment extends Fragment {

	public static CustomTabLayout tabpager;
	public static ViewPager pager;

	public static ProgressBar loading;
	public static LinearLayout retry;
	static Button btnReload;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

		loading   = (ProgressBar) rootView.findViewById(R.id.pgbarLoading);
		retry     = (LinearLayout) rootView.findViewById(R.id.loadMask);
		btnReload = (Button) rootView.findViewById(R.id.btnReload);

		tabpager  = (CustomTabLayout) rootView.findViewById(R.id.tab_layout);
		pager     = (ViewPager) rootView.findViewById(R.id.pager);

        btnReload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((MainActivity) getActivity()).loadDataDashboard();
			}
		});

		pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabpager));
		tabpager.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				final int position = tab.getPosition();
				pager.setCurrentItem(position, true);
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {

			}
		});

		return rootView;
	}

	@Override
	public void onStop() {

		super.onStop();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((MainActivity) getActivity()).loadDataDashboard();

	}

	public static void resultLoadDashboard(Context context) {

		tabpager.removeAllTabs();
		tabpager.setTabMode(TabLayout.MODE_FIXED);
		tabpager.addTab(tabpager.newTab().setText("KARTA PEDIA"));
		tabpager.addTab(tabpager.newTab().setText("KARTA PAYSHOP"));
		tabpager.addTab(tabpager.newTab().setText("KARTA NEWS"));
		tabpager.addTab(tabpager.newTab().setText("KARTA CARE"));

		PagerMenuAdapter pagerMenuAdapter = new PagerMenuAdapter(((MainActivity) context).getSupportFragmentManager());
		pager.setAdapter(pagerMenuAdapter);

	}


}
