
package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;


public class MetodePembayaranFragment extends Fragment {

	public static ScrollView detail_view;
	public static MyTextView total_tagihan;
	public static LinearLayout linear_metode_saldo, linear_metode_transfer_bank;
	public static CheckBox metode_bayar_transfer_bank, metode_bayar_saldo;
	public static ListView listViewBank;
	public static MyTextView lanjutkan;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.fragment_metode_pembayaran, container, false);

		detail_view = (ScrollView) rootView.findViewById(R.id.detail_view);
		total_tagihan = (MyTextView) rootView.findViewById(R.id.total_tagihan);
		linear_metode_saldo = (LinearLayout) rootView.findViewById(R.id.linear_metode_saldo);
		linear_metode_transfer_bank = (LinearLayout) rootView.findViewById(R.id.linear_metode_transfer_bank);
		total_tagihan = (MyTextView) rootView.findViewById(R.id.total_tagihan);
		listViewBank = (ListView) rootView.findViewById(R.id.lisview);
		lanjutkan    = (MyTextView) rootView.findViewById(R.id.next);
		metode_bayar_transfer_bank = (CheckBox) rootView.findViewById(R.id.metode_bayar_transfer_bank);
		metode_bayar_saldo = (CheckBox) rootView.findViewById(R.id.metode_bayar_saldo);

		metode_bayar_transfer_bank.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				metode_bayar_transfer_bank.setChecked(true);
				metode_bayar_saldo.setChecked(false);
				((ProsesPpobActivity) getActivity()).setMetodePembayaran(1);
			}
		});

		metode_bayar_saldo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				metode_bayar_transfer_bank.setChecked(false);
				metode_bayar_saldo.setChecked(true);
				((ProsesPpobActivity) getActivity()).setMetodePembayaran(2);
			}
		});

		lanjutkan.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String message = ((ProsesPpobActivity) getActivity()).checkedMetodePembayaranBeforeNext();
				if(message.length()==0) {
					((ProsesPpobActivity) getActivity()).loadDataPesanan();
					((ProsesPpobActivity) getActivity()).gotoPage(3);
				} else {
					((ProsesPpobActivity) getActivity()).openDialogMessage(message, false);
				}
			}
		});

		((ProsesPpobActivity) getActivity()).setInitialMetodePembayaran();
		((ProsesPpobActivity) getActivity()).loadDataBank();

		((ProsesPpobActivity) getActivity()).loadFieldTotalTransfer();

		return rootView;
	}
	
}
