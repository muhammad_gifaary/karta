
package karangtarunaku.application.com.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.ExpandableHeightListView;

public class KonfirmasiPemesananFragment extends Fragment {


	public static ScrollView detail_view;
	public static ImageView image_pembayaran;
	public static MyTextView metode_pembayaran;
	public static MyTextView detail_pembayaran;
	public static ExpandableHeightListView listViewPesanan;
	public static MyTextView total;
	public static MyTextView selesai;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.fragment_konfirmasi_pemesanan, container, false);

		detail_view = (ScrollView) rootView.findViewById(R.id.detail_view);
		metode_pembayaran = (MyTextView) rootView.findViewById(R.id.metode_pembayaran);
		image_pembayaran = (ImageView) rootView.findViewById(R.id.image_pembayaran);
		detail_pembayaran = (MyTextView) rootView.findViewById(R.id.detail_pembayaran);
		listViewPesanan = (ExpandableHeightListView) rootView.findViewById(R.id.lisview);

		total      = (MyTextView) rootView.findViewById(R.id.total);
		selesai    = (MyTextView) rootView.findViewById(R.id.selesai);

		selesai.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((ProsesPpobActivity) getActivity()).submitPpob();
			}
		});

		((ProsesPpobActivity) getActivity()).setInitialKonfirmasiPesanan();
		((ProsesPpobActivity) getActivity()).loadDataPesanan();

		return rootView;
	}
	
}
