package karangtarunaku.application.com.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;

import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;

public class PembayaranFragment extends Fragment {

	SwipeRefreshLayout swipe_container;

	public static GridView gridView;
    public static LinearLayout retry;
	public static Button btnReload;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_pembayaran, container, false);

		swipe_container  = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        gridView         = (GridView) rootView.findViewById(R.id.gridView);
		retry            = (LinearLayout) rootView.findViewById(R.id.loadMask);
		btnReload        = (Button) rootView.findViewById(R.id.btnReload);

		swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

			@Override
			public void onRefresh() {
				swipe_container.setRefreshing(false);

			}
		});

        btnReload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub


			}
		});

        return rootView;
    }

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);


	}
}
