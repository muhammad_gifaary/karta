package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.DetailKartagramActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.SliderLayout;

public class DetailKartagramFragment extends Fragment {

	public static SliderLayout mProdukSlider;
	public static MyTextView tanggal;
	public static MyTextView oleh;

	//public static ProgressBar loading;
	//public static LinearLayout retry;
	//public static Button btnReload;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.fragment_detail_kartagram, container, false);

		mProdukSlider    = (SliderLayout) rootView.findViewById(R.id.slider);
		tanggal = (MyTextView) rootView.findViewById(R.id.tanggal);
		oleh = (MyTextView) rootView.findViewById(R.id.atlit);

		((DetailKartagramActivity) getActivity()).LoadDetailKartagram();

		return rootView;
	}

}
