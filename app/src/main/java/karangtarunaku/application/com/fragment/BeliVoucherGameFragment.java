package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import customfonts.MyEditText;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;

public class BeliVoucherGameFragment extends Fragment {


    public static MyEditText edit_jenis;

    public static ListView listViewVoucher;
    public static RelativeLayout linear_voucher;
    float downX = 0, downY = 0, upX, upY;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_voucher_game, container, false);

        edit_jenis = (MyEditText) rootView.findViewById(R.id.edit_jenis);
        edit_jenis.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downX = event.getX();
                        downY = event.getY();

                        break;

                    case MotionEvent.ACTION_UP:
                        upX = event.getX();
                        upY = event.getY();
                        float deltaX = downX - upX;
                        float deltaY = downY - upY;

                        if(Math.abs(deltaX)<50 && Math.abs(deltaY)<50) {
                            ((ProsesPpobActivity) getActivity()).loadDialogListView("game");
                        }

                        break;
                }

                return false;
            }
        });

        linear_voucher = (RelativeLayout) rootView.findViewById(R.id.linear2);
        listViewVoucher = (ListView) rootView.findViewById(R.id.lisview);

        edit_jenis.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                ((ProsesPpobActivity) getActivity()).loadDataVoucherGame();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });


        ((ProsesPpobActivity) getActivity()).loadDataJenisVoucherGame();

		return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
