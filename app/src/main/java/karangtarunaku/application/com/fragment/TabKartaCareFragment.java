package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.ChildAnimationExample;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.ExpandableHeightGridView;
import karangtarunaku.application.com.libs.SliderLayout;
import karangtarunaku.application.com.model.banner;

/**
 * Created by gifary on 10/10/18.
 */

public class TabKartaCareFragment extends Fragment {
    static SliderLayout mBannerSlider;
    static ExpandableHeightGridView gridViewTerbaru;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tab_kartacare, container, false);

        mBannerSlider   = (SliderLayout) rootView.findViewById(R.id.slider);

        gridViewTerbaru = (ExpandableHeightGridView) rootView.findViewById(R.id.gridViewTerbaru);

        mBannerSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mBannerSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mBannerSlider.setCustomAnimation(new ChildAnimationExample());
        mBannerSlider.setDuration(4000);
        mBannerSlider.addOnPageChangeListener((MainActivity) getActivity());

        for (banner data_banner : MainActivity.dashboard_list_banner) {

            TextSliderView textSliderView = new TextSliderView(getActivity().getApplicationContext());
            textSliderView
                    .image(CommonUtilities.SERVER_URL+"/uploads/banner/"+data_banner.getUrl_image())
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop);

            mBannerSlider.addSlider(textSliderView);
        }

        gridViewTerbaru.setAdapter(MainActivity.careAdapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
