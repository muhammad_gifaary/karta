package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import customfonts.MyEditText;
import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;

public class BayarListrikFragment extends Fragment {

    public static MyEditText edit_id_pel;
    public static MyTextView cek_tagihan;

    public static LinearLayout linear_detail_tagihan;
    public static LinearLayout linear_bayar;

    public static MyEditText edit_customer;
    public static MyEditText edit_nomer;
    public static MyEditText edit_tarif;
    public static MyEditText edit_daya;
    public static MyEditText edit_periode;
    public static MyEditText edit_tagihan;
    public static MyEditText edit_denda;
    public static MyEditText edit_admin;
    public static MyEditText edit_total;

    public static MyTextView bayar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_bayar_tagihan_listrik, container, false);

        edit_id_pel = (MyEditText) rootView.findViewById(R.id.edit_id_pel);
        cek_tagihan = (MyTextView) rootView.findViewById(R.id.cek_tagihan);

        cek_tagihan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((ProsesPpobActivity) getActivity()).cekTagihanListrik(edit_id_pel.getText().toString());
            }
        });

        linear_detail_tagihan = (LinearLayout) rootView.findViewById(R.id.linear2);
        linear_detail_tagihan.setVisibility(View.GONE);

        edit_customer = (MyEditText) rootView.findViewById(R.id.edit_customer);
        edit_nomer = (MyEditText) rootView.findViewById(R.id.edit_nomer);
        edit_tarif = (MyEditText) rootView.findViewById(R.id.edit_tarif);
        edit_daya = (MyEditText) rootView.findViewById(R.id.edit_daya);
        edit_periode = (MyEditText) rootView.findViewById(R.id.edit_periode);
        edit_tagihan = (MyEditText) rootView.findViewById(R.id.edit_tagihan);
        edit_denda = (MyEditText) rootView.findViewById(R.id.edit_denda);
        edit_admin = (MyEditText) rootView.findViewById(R.id.edit_admin);
        edit_total = (MyEditText) rootView.findViewById(R.id.edit_total);

        linear_bayar = (LinearLayout) rootView.findViewById(R.id.linear_bayar);
        linear_bayar.setVisibility(View.GONE);

        bayar = (MyTextView) rootView.findViewById(R.id.bayar);

        bayar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String message = ((ProsesPpobActivity) getActivity()).checkedBayarListrikBeforeNext();
                if(message.length()==0) {
                    ((ProsesPpobActivity) getActivity()).loadFieldTotalTransfer();
                    ((ProsesPpobActivity) getActivity()).gotoPage(2);
                } else {
                    ((ProsesPpobActivity) getActivity()).openDialogMessage(message, false);
                }
            }
        });


        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
