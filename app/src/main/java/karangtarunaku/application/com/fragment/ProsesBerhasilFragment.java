
package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;


public class ProsesBerhasilFragment extends Fragment {


	public static MyTextView keterangan;
	public static MyTextView konfirmasi_pembayaran;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View rootView = inflater.inflate(R.layout.fragment_proses_berhasil, container, false);

		keterangan  = (MyTextView) rootView.findViewById(R.id.keterangan);
		konfirmasi_pembayaran = (MyTextView) rootView.findViewById(R.id.selesai);

		konfirmasi_pembayaran.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((ProsesPpobActivity) getActivity()).open_konfirmasi_page();
			}
		});

		return rootView;
	}
	
}
