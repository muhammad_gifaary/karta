package karangtarunaku.application.com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.MainActivity;
import karangtarunaku.application.com.karangtarunaku.R;
import karangtarunaku.application.com.libs.ChildAnimationExample;
import karangtarunaku.application.com.libs.CommonUtilities;
import karangtarunaku.application.com.libs.EndlessScrollListener;
import karangtarunaku.application.com.libs.ExpandableHeightListView;
import karangtarunaku.application.com.libs.ResizableImageView;
import karangtarunaku.application.com.libs.SliderLayout;
import karangtarunaku.application.com.model.banner;
import karangtarunaku.application.com.model.news;

public class TabKartanewsFragment extends Fragment {

    static SliderLayout mSlider;
    static ListView listview;


    news temp = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_tab_kartanews, container, false);

        listview = (ListView) rootView.findViewById(R.id.listview);

        LayoutInflater inflater_header = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header_tab_kartanews, listview, false);
        mSlider = (SliderLayout) header.findViewById(R.id.newsslider);

        mSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mSlider.setCustomAnimation(new ChildAnimationExample());
        mSlider.setDuration(4000);
        mSlider.addOnPageChangeListener((MainActivity) getActivity());

        //banner
        mSlider.removeAllSliders();
        for (news data_news : MainActivity.kartanewsterbarulist) {

            MyTextSliderView textSliderView = new MyTextSliderView(getActivity().getApplicationContext(), data_news);
            mSlider.addSlider(textSliderView);
        }

        listview.addHeaderView(header, null, false);
        listview.setAdapter(MainActivity.kartanewsAdapter);

        listview.setOnScrollListener(new EndlessScrollListener() {

            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {

                ((MainActivity) getActivity()).loadDataKartanews();

                return true;
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public class MyTextSliderView extends BaseSliderView {

        news data;

        public MyTextSliderView(Context context, news data) {
            super(context);
            this.data = data;
        }

        @Override
        public View getView() {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.header_news_item_list,null);
            ImageView gambar = (ImageView)v.findViewById(R.id.gambar);
            MyTextView judul = (MyTextView)v.findViewById(R.id.judul);
            MyTextView kategori = (MyTextView)v.findViewById(R.id.kategori);
            MyTextView tanggal = (MyTextView)v.findViewById(R.id.tanggal);

            String server = CommonUtilities.SERVER_URL;
            String url = server+"/uploads/kartanews/"+data.getGambar();
            MainActivity.imageLoader.displayImage(url, gambar, MainActivity.imageOptionKartanews);

            judul.setText(data.getJudul().toUpperCase());
            kategori.setText(data.getKategori().toUpperCase());
            tanggal.setText(data.getSejak().toUpperCase());

            return v;
        }
    }

}
