package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import customfonts.MyEditText;
import customfonts.MyTextView;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;

public class DepositSaldoFragment extends Fragment {

    public static MyEditText edit_nominal;
    public static MyTextView lanjutkan;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_deposit_saldo, container, false);

        edit_nominal = (MyEditText) rootView.findViewById(R.id.edit_nominal);
        lanjutkan = (MyTextView) rootView.findViewById(R.id.next);

        lanjutkan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String message = ((ProsesPpobActivity) getActivity()).checkedDepositSaldoBeforeNext();
                if(message.length()==0) {
                    //((ProsesPpobActivity) getActivity()).setTotalDeposit();
                    ((ProsesPpobActivity) getActivity()).loadFieldTotalTransfer();
                    ((ProsesPpobActivity) getActivity()).gotoPage(2);
                } else {
                    ((ProsesPpobActivity) getActivity()).openDialogMessage(message, false);
                }
            }
        });

		return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
