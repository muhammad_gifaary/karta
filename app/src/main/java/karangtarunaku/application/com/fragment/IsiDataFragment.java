package karangtarunaku.application.com.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import customfonts.MyEditText;
import karangtarunaku.application.com.karangtarunaku.ProsesPpobActivity;
import karangtarunaku.application.com.karangtarunaku.R;

public class IsiDataFragment extends Fragment {


    public static MyEditText edit_nohp;
    public static ImageView phonebook;
    public static ListView listViewData;
    public static RelativeLayout linear_data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_isi_data, container, false);

        edit_nohp = (MyEditText) rootView.findViewById(R.id.edit_nohp);
        phonebook = (ImageView) rootView.findViewById(R.id.phonebook);

        linear_data = (RelativeLayout) rootView.findViewById(R.id.linear2);
        listViewData = (ListView) rootView.findViewById(R.id.lisview);

        edit_nohp.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                ((ProsesPpobActivity) getActivity()).loadDataData(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        phonebook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((ProsesPpobActivity) getActivity()).pickPhoneBook();
            }
        });
		return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
